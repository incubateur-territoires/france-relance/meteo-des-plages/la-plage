## En bref

_En naviguant sur [LaPlage.info](https://laplage.info/) j'ai rencontré le problème suivant :_

## Qu'auriez-vous attendu à la place ?

_Avez-vous une solution à suggérer ?_

## Capture d'écran

_Pouvez-vous ajouter une capture d'écran ?_

## Message d'erreur

_Avez-vous rencontré un message d'erreur en particulier ?_

## Contexte

_Avez-vous rencontré le problème sur mobile ?_

_Quel est votre navigateur Internet ?_

**:raised_hands: Merci pour votre contribution !**

/label ~source::LaPlage.info
