import type { GatsbyConfig } from 'gatsby';

const siteUrl = 'https://laplage.info';

const config: GatsbyConfig = {
  siteMetadata: {
    title: `La Plage.info`,
    siteUrl,
  },
  trailingSlash: 'always',
  // More easily incorporate content into your pages through automatic TypeScript type generation and better GraphQL IntelliSense.
  // If you use VSCode you can also use the GraphQL plugin
  // Learn more at: https://gatsby.dev/graphql-typegen
  graphqlTypegen: true,
  plugins: [
    'gatsby-plugin-emotion',
    'gatsby-plugin-image',
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-sitemap',
    {
      resolve: 'gatsby-plugin-sitemap',
      options: {
        serialize: ({ path }: { path: string; modifiedGmt: string }) => ({
          url: path,
          lastmod: new Date().toISOString(),
        }),
      },
    },
    {
      resolve: 'gatsby-plugin-manifest',
      options: {
        icon: 'src/images/icon.png',
      },
    },
    'gatsby-plugin-mdx',
    'gatsby-plugin-sharp',
    'gatsby-transformer-sharp',
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'images',
        path: './src/images/',
      },
      __key: 'images',
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'pages',
        path: './src/pages/',
      },
      __key: 'pages',
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `markdown-pages`,
        path: `${__dirname}/src/markdown-pages`,
      },
      __key: 'markdown-pages',
    },
    `gatsby-transformer-remark`,
    {
      resolve: 'gatsby-plugin-matomo',
      options: {
        siteId: '4',
        matomoUrl: 'https://matomo.collectivite.org/',
        siteUrl,
        disableCookies: true,
      },
    },
    `gatsby-theme-material-ui`,
  ],
};

export default config;
