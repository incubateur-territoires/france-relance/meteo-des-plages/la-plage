import fs from 'fs/promises';
import csvtojson from 'csvtojson';
import simovSlugify from 'slugify';

const slugify = (string) => simovSlugify(string, { strict: true, lower: true });

const villesFilepath = 'Textes SEO __ Météo des plages - villes.csv';
const plagesFilepath = 'Textes SEO __ Météo des plages - plages.csv';
(async () => {
  const villes = await csvtojson().fromFile(villesFilepath);
  for (const { departement, nom, title, description, corps } of villes) {
    const directory = `src/markdown-pages/${departement}/`;
    await fs.mkdir(directory, { recursive: true });
    await fs.writeFile(
      `${directory}${slugify(nom)}.md`,
      `---
title: "${title}"
description: "${description}"
---

${corps}
`
    );
  }

  const plages = await csvtojson().fromFile(plagesFilepath);
  for (const { departement, ville, nom, title, description, corps } of plages) {
    if (!corps) {
      continue;
    }

    const directory = `src/markdown-pages/${departement}/${slugify(ville)}/`;
    await fs.mkdir(directory, { recursive: true });
    await fs.writeFile(
      `${directory}${slugify(nom)}.md`,
      `---
title: "${title}"
description: "${description}"
---

${corps}
`
    );
  }
})();
