import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';
import { Link } from 'gatsby-theme-material-ui';
import { DepartementInterface } from '../model/DepartementInterface';

export default function DepartementCard({ departement: { numero, nom } }: { departement: DepartementInterface }) {
  return (
    <Card>
      <CardActionArea component={Link} to={`/${numero}`}>
        <CardContent>
          <Typography gutterBottom variant="h3">
            {nom}
          </Typography>
          <Typography variant="subtitle1">{numero}</Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}
