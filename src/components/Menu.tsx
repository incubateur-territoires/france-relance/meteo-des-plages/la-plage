import * as React from 'react';
import { Button, Link } from 'gatsby-theme-material-ui';
import {
  Divider,
  Menu as MuiMenu,
  MenuItem,
  PopoverProps,
  ListItemText,
  ListItemIcon,
  ListItem,
  ListSubheader,
} from '@mui/material';
import { Home } from '@mui/icons-material/';

export interface MenuProps {
  anchorEl: PopoverProps['anchorEl'];
  open: boolean;
  handleClose: () => void;
}

export default function Menu({ anchorEl, open, handleClose }: MenuProps) {
  return (
    <MuiMenu anchorEl={anchorEl} open={open} onClose={handleClose}>
      <MenuItem component={Link} to="/">
        <ListItemIcon>
          <Home fontSize="small" />
        </ListItemIcon>
        <ListItemText>Accueil</ListItemText>
      </MenuItem>
      <Divider />
      <ListSubheader>Espace réservé aux collectivités</ListSubheader>
      <ListItem sx={{ display: 'flex', justifyContent: 'center' }}>
        <Button
          variant="contained"
          disableElevation
          component={Link}
          target="_blank"
          rel="noopener"
          href="https://www.frequence-sud.fr/errorconnect.php"
          sx={{ textTransform: 'none' }}
        >
          Connexion professionnels
        </Button>
      </ListItem>
      <ListItem sx={{ display: 'flex', justifyContent: 'center' }}>
        <Link target="_blank" rel="noopener" href="https://www.frequence-sud.fr/pro/new_user_pro.php">
          Inscription
        </Link>
      </ListItem>
    </MuiMenu>
  );
}
