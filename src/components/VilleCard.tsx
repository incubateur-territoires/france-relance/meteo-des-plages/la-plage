import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';
import { Link } from 'gatsby-theme-material-ui';
import { VilleNodeInterface } from '../infrastructure/model/Gatsby/VilleNodeInterface';
import { Ville } from '../model/Ville';

export default function VilleCard({ ville: { nom, departement, plages } }: { ville: VilleNodeInterface }) {
  return (
    <Card>
      <CardActionArea component={Link} to={Ville.getPath({ departement, nom })}>
        <CardContent>
          <Typography gutterBottom variant="h2">
            {nom}
          </Typography>
          <Typography variant="subtitle1">{plages.length} plage(s)</Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}
