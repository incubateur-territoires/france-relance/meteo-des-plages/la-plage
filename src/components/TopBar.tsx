import * as React from 'react';
import { AppBar, Box } from '@mui/material';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import { useScrollTrigger } from '@mui/material';
import LogoSvg from './LogoSvg';
import { Link } from 'gatsby-theme-material-ui';
import Menu from './Menu';

interface ElevationScrollProps {
  children: React.ReactNode;
}

function ElevationScroll(props: ElevationScrollProps) {
  const { children } = props;
  // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  // This is only being set here because the demo is in an iframe.
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0,
  });

  // @ts-ignore
  return React.cloneElement(children, {
    elevation: trigger ? 4 : 1,
  });
}

export default function TopBar(props: ElevationScrollProps) {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const menuOpen = Boolean(anchorEl);
  const handleMenuOpen = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleMenuClose = () => setAnchorEl(null);

  return (
    <ElevationScroll {...props}>
      <AppBar color="neutral">
        <Toolbar>
          <Box sx={{ flexGrow: 1 }}>
            <Link to="/">
              <LogoSvg />
            </Link>
          </Box>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
            aria-controls={menuOpen ? 'basic-menu' : undefined}
            aria-haspopup="true"
            aria-expanded={menuOpen ? 'true' : undefined}
            onClick={handleMenuOpen}
          >
            <MenuIcon />
          </IconButton>
          <Menu
            anchorEl={anchorEl}
            open={menuOpen}
            handleClose={handleMenuClose}
            MenuListProps={{
              'aria-labelledby': 'basic-button',
            }}
          />
        </Toolbar>
      </AppBar>
    </ElevationScroll>
  );
}
