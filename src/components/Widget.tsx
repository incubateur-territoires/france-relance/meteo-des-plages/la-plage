import React from 'react';
import { css } from '@emotion/react';
import { PlageInterface } from '../model/PlageInterface';

interface WidgetProps {
  plageId: PlageInterface['frequenceSudId'];
}

const Widget = ({ plageId }: WidgetProps) => {
  return (
    <iframe
      id="widget_plage"
      css={css`
        width: 100%;
        min-width: 300px;
        height: 200px;
        min-height: 200px;
        display: block;
        border-width: 0;
      `}
      scrolling="no"
      src={`https://www.frequence-sud.fr/rss/ext_display/sanary/plage_detail.php?id_plage=${plageId}`}
    ></iframe>
  );
};

export default Widget;
