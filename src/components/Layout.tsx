import React from 'react';
import TopBar from './TopBar';
import { Box, Container, Stack } from '@mui/material';
import Toolbar from '@mui/material/Toolbar';
import { Link } from 'gatsby-theme-material-ui';

export default function Layout({ children }: { children: React.ReactNode }) {
  return (
    <>
      <TopBar>{children}</TopBar>
      <Toolbar />
      <Container>
        <Box sx={{ my: 2 }}>{children}</Box>
      </Container>
      <Box component="footer" sx={{ width: '100%' }}>
        <Stack sx={{ backgroundColor: '#313131' }}>
          <Box sx={{ padding: 2, textAlign: 'center', color: 'white' }}>Contactez-nous</Box>
          <Box sx={{ padding: 2, textAlign: 'center', color: 'white' }}>Mention légales</Box>
          <Box
            sx={{ padding: 2, textAlign: 'center', color: 'white' }}
            component={Link}
            target="_blank"
            rel="noopener"
            href="https://gitlab.com/incubateur-territoires/france-relance/meteo-des-plages/la-plage/-/issues/new?issue[title]=Lors%20de%20ma%20navigation...&issuable_template=ProblemePlageInfo"
          >
            Signaler un problème
          </Box>
        </Stack>
      </Box>
    </>
  );
}
