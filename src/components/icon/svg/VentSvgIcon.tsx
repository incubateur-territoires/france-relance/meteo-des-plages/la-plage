import React from 'react';

export default function VentSvgIcon() {
  return (
    <svg width="39" height="40" viewBox="0 0 39 40" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M7.0647 6.42775V33.9541" stroke="#F1BC95" strokeWidth="2.03899" strokeLinecap="round" />
      <path
        d="M11.7404 8.91215C11.7404 7.62637 12.9159 6.66154 14.177 6.91232L30.9811 10.254C31.9352 10.4438 32.6224 11.2811 32.6224 12.2539V14.292C32.6224 15.244 31.9637 16.0693 31.0353 16.2803L14.2312 20.0994C12.9553 20.3894 11.7404 19.4196 11.7404 18.1111V8.91215Z"
        stroke="#F1BC95"
        strokeWidth="2.03899"
      />
      <path d="M18.3848 7.37693V19.7163M25.9782 9.22785V17.8654" stroke="#F1BC95" strokeWidth="2.03899" />
    </svg>
  );
}
