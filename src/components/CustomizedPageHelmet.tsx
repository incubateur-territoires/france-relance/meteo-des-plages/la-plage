import React from 'react';
import { Helmet } from 'react-helmet';
import { PageMetadata } from '../model/Page/PageCustomizationInterface';

interface CustomizedPageHelmetProps {
  metadata?: PageMetadata;
  defaultTitle?: Queries.Maybe<string>;
}

const CustomizedPageHelmet = ({ metadata, defaultTitle }: CustomizedPageHelmetProps) => {
  return (
    <Helmet
      htmlAttributes={{ lang: 'fr' }}
      title={metadata?.title}
      titleTemplate={defaultTitle ? `%s | ${defaultTitle}` : null}
      meta={[
        {
          name: `description`,
          content: metadata?.description,
        },
        {
          property: `og:title`,
          content: metadata?.title,
        },
        {
          property: `og:description`,
          content: metadata?.description,
        },
        {
          property: `og:type`,
          content: `website`,
        },
        {
          name: `twitter:card`,
          content: `summary`,
        },
        {
          name: `twitter:title`,
          content: metadata?.title,
        },
        {
          name: `twitter:description`,
          content: metadata?.description,
        },
      ]}
    />
  );
};

export default CustomizedPageHelmet;
