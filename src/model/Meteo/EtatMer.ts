export enum EtatMer {
  Calme = 'calme',
  Ridee = 'ridée',
  Belle = 'belle',
  PeuAgitee = 'peu agitée',
  Agitee = 'agitée',
  Forte = 'forte',
  HouleSignificative = 'houle significative',
  ForteHoule = 'forte houle',
}
