import { EauInterface } from './EauInterface';
import { QualiteEau } from './QualiteEau';

export class Eau implements EauInterface {
  public temperature?: number;
  public qualite: QualiteEau;

  public constructor({ temperature, qualite }: EauInterface) {
    this.temperature = temperature;
    this.qualite = qualite;
  }

  static getVitesseFormatee(vitesse: number) {
    return `${vitesse} km/h`;
  }
}
