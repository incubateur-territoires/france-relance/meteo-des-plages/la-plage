export enum QualiteEau {
  Excellente = 'Excellente',
  Bonne = 'Bonne',
  Moyenne = 'Moyenne',
  Mauvaise = 'Mauvaise',
  Pollution = 'Pollution',
  AnalyseEnCours = 'Analyse en cours',
  AbsenceAnalyse = "Absence d'analyse",
}
