export enum NiveauDeRisque {
  FORT = 'Fort',
  FAIBLE = 'Faible',
  LIMITE = 'Marqué ou limité',
}
