import { NiveauDeRisque } from './NiveauDeRisque';
import { TypeDeDrapeau } from './TypeDeDrapeau';

export interface DrapeauInterface {
  typeDeDrapeau: TypeDeDrapeau;
  niveauDeRisque?: NiveauDeRisque;
  signification: string;
  codeCouleur: string;
}
