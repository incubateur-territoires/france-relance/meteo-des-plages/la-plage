export enum ConditionMeteoCategorie {
  Orage = 'Orage',
  Bruine = 'Bruine',
  Pluie = 'Pluie',
  Neige = 'Neige',
  Atmosphere = 'Atmosphere',
  Clair = 'Clair',
  Nuage = 'Nuage',
}
