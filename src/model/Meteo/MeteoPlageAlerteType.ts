export enum MeteoPlageAlerteType {
  PresenceMeduses = 'Présence signalée de méduses',
  PresenceRaies = 'Présence signalée de raies',
  CourantsDangereux = 'Courants dangereux',
  DegradationMeteo = 'Dégradation météo prévue dans la journée',
  Inondations = 'Inondations',
  NiveauEauFaible = "Niveau de l'eau faible",
  PlageFermeAuPublic = 'Plage fermée au public',
}
