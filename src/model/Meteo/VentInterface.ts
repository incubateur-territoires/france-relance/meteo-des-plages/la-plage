export interface VentInterface {
  vitesse: number; // km/h
  direction: number; // angle en degré
}
