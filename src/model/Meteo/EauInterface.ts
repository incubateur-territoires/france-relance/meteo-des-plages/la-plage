import { QualiteEau } from './QualiteEau';

export interface EauInterface {
  temperature?: number; // ° Celsius
  qualite: QualiteEau;
}
