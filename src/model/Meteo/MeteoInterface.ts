import { TypeDeDrapeau } from './Drapeau/TypeDeDrapeau';
import { PlageInterface } from '../PlageInterface';
import { MeteoPlageAlerteType } from './MeteoPlageAlerteType';
import { EtatMer } from './EtatMer';
import { MeteoSourceType } from './MeteoSourceType';
import { VentInterface } from './VentInterface';
import { EauInterface } from './EauInterface';

export interface MeteoInterface {
  plageId: PlageInterface['id'];
  openWeatherMapCode: number; // https://openweathermap.org/weather-conditions
  temperature: number; // ° Celsius
  vent: VentInterface;
  derniereMiseAJour: Date;
  typeDeDrapeau?: TypeDeDrapeau;
  eau: EauInterface;
  merEtat?: EtatMer;
  alertes: MeteoPlageAlerteType[];
  commentaire: string; // commentaire libre
  sourceType: MeteoSourceType;
}
