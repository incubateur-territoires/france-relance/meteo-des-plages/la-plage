import { CoordinatesInterface } from './CoordinatesInterface';

export class Coordinates implements CoordinatesInterface {
  public latitude: number;
  public longitude: number;

  public constructor({ latitude, longitude }: CoordinatesInterface) {
    this.latitude = latitude;
    this.longitude = longitude;
  }

  static toString({ latitude, longitude }: CoordinatesInterface) {
    return `[${latitude}, ${longitude}]`;
  }
}
