export class Entity<EntityInterface extends Record<string, unknown>> {
  constructor(entity: EntityInterface) {
    Object.assign(this, entity);
  }
}
