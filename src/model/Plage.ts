import { slugify } from '../infrastructure/slugify';
import { PlageInterface } from './PlageInterface';
import { VilleInterface } from './VilleInterface';
import { CoordinatesInterface } from './CoordinatesInterface';
import { MeteoInterface } from './Meteo/MeteoInterface';
import { Ville } from './Ville';

export class Plage implements PlageInterface {
  public id: string;
  public frequenceSudId: number;
  public email?: string;
  public coordinates: CoordinatesInterface;
  public nom: string;
  public siteWeb?: string;
  public tags: string[];
  public telephone?: string;
  public ville: VilleInterface;
  public villeId: string;
  public meteo?: MeteoInterface;

  public constructor(plage: PlageInterface) {
    this.id = plage.id;
    this.frequenceSudId = plage.frequenceSudId;
    this.nom = plage.nom;
    this.coordinates = plage.coordinates;
    this.ville = plage.ville;
    this.villeId = plage.villeId;
    this.telephone = plage.telephone;
    this.email = plage.email;
    this.siteWeb = plage.siteWeb;
    this.tags = plage.tags;
    this.meteo = plage.meteo;
  }

  public static computeId(nom: Plage['nom']) {
    return `Plage-${Plage.normalizeNom(nom)}`;
  }

  public static normalizeNom(nom: Plage['nom']) {
    return slugify(nom);
  }

  getPath() {
    return Plage.getPath(this);
  }

  static getPath(plage: PlageInterface) {
    return `/${plage.ville.departement}/${Ville.normalizeNom(plage.ville.nom)}/${plage.id}/${Plage.normalizeNom(
      plage.nom
    )}`;
  }
}
