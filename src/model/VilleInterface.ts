import { CoordinatesInterface } from './CoordinatesInterface';

export interface VilleInterface {
  id: string;
  nom: string;
  coordinates: CoordinatesInterface;
  departement: string;
  codePostal: number;
}
