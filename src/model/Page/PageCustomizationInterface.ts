export interface PageMetadata {
  title?: string;
  description?: string;
}

export interface PageCustomizationInterface {
  titre?: string;
  description?: string;
  metadata?: PageMetadata;
}
