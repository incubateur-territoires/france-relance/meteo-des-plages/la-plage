import { VilleInterface } from './VilleInterface';
import { CoordinatesInterface } from './CoordinatesInterface';
import { MeteoInterface } from './Meteo/MeteoInterface';

export interface PlageInterface {
  id: string;
  frequenceSudId: number;
  nom: string;
  coordinates: CoordinatesInterface;
  ville: VilleInterface;
  villeId: string;
  telephone?: string;
  email?: string;
  siteWeb?: string;
  tags: string[];
  meteo?: MeteoInterface;

  // plageId: PlageInterface['id'];
  // openWeatherMapCode: number; // https://openweathermap.org/weather-conditions
  // temperature: number; // ° Celsius
  // derniereMiseAJour: Date;
  // typeDeDrapeau?: TypeDeDrapeau;
  // merEtat?: EtatMer;
  // alertes: MeteoPlageAlerteType[];
  // commentaire: string; // commentaire libre
  // sourceType: MeteoPlageSourceType;
}
