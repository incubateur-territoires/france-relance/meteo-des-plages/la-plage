import { DepartementInterface } from './DepartementInterface';

export const departements: DepartementInterface[] = [
  {
    numero: '6',
    nom: 'Alpes-Maritimes',
  },
  {
    numero: '13',
    nom: 'Bouches-du-Rhône',
  },
  {
    numero: '83',
    nom: 'Var',
  },
];
