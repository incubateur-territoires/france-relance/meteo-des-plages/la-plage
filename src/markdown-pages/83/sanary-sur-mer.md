---
title: "Sanary-sur-Mer, station balnéaire | Météo des Plages"
description: "Sanary-sur-Mer est une station balnéaire dynamique qui se situe dans le Var en région Provence-Alpes-Côte d'Azur (83). Profitez de ses belles plages."
---

# La station balnéaire de Sanary-sur-Mer

**Sanary-sur-Mer** est une station balnéaire qui se situe dans le **département du Var (83) en région Provence-Alpes-Côte d’Azur (PACA)**. Cette ville est à proximité de Toulon (13 km) et de Marseille (49 km). Les habitants de Sanary sont appelés les sanaryens et ils sont plus de 17 000.

Haute qualité de vie, commerces de proximité, activités de loisirs… **Sanary** est une ville dynamique où vous ne pourrez pas vous ennuyer. C’est un lieu incontournable de l’arrière-pays varois. Profitez de 8 km de plages, de multiples criques, d’un sentier du littoral bordé d’essence méditerranéenne ainsi que de magnifiques jardins.

## Comment accéder à Sanary-sur-Mer ?

Pour rejoindre la **ville de Sanary-sur-Mer**, plusieurs options s’offre à vous : voie routière ou transports en commun.

### Venir à Sanary-sur-Mer en voiture

L’accès routier se fait par la route départementale RD 559 entre **Six-Fours-les-Plages et Bandol**. Depuis Toulon, la sortie de l’autoroute A50 la plus proche de **Sanary-sur-Mer** est la sortie 13 et depuis Marseille, c’est la sortie 12.

### Venir à Sanary-sur-Mer en train

Si vous venez en transports en communs, la gare TGV la plus proche est celle de Toulon. Il y a également la gare d’Ollioules – Sanary, qui est une gare TER : il n’y a donc que les trains TER Provence-Alpes-Côte d’Azur qui y circulent.

Sanary possède également un réseau d’autocar : le réseau départemental Varlib. Vous y trouverez la ligne 8805 qui dessert le trajet Bandol – Sanary – Toulon et la ligne 8806 pour le trajet Bandol – Sanary – La Seyne. Les autres autocars (8826, 8832, et 8852) sont des bus scolaires.

### Venir à Sanary-sur-Mer en avion

Dans le cas où vous viendrez en avion jusqu’à **Sanary**, les aéroports les plus proches sont l’aéroport de Toulon-Hyères, celui de Marseille-Provence, et celui de Nice-Côte d’Azur.

## Où se promener à Sanary-sur-Mer dans le Var ?

**Sanary** est une ville très riche en loisirs. La ville est dynamique et organise de nombreux événements tout au long de la saison estivale, c’est l’endroit idéal pour partir en vacances. Découvrez l’**authenticité provençale de Sanary**, avec ses ruelles piétonnes remplies de charme.

### Les lieux incontournables

**Sanary-sur-Mer** est une destination touristique idéale durant la saison estivale. Les activités ne manquent pas ! La **tour de Sanary** vous offrira un magnifique panorama sur toute la ville et l’accès y est gratuit. Si vous venez en famille, faites profiter vos enfants du parc animalier et exotique Zoa. La visite dure entre 1h et 2h et vous aurez l’occasion de voir de nombreux animaux, donc certaines espèces en voie de disparition. N’hésitez pas à faire un tour à l’église Saint Nazaire ainsi qu’à la chapelle Notre-Dame de Pitié.

**Sanary** vous offre également de belles balades le long de son port ainsi que des marchés.  Le marché des Forains est très connu : il a lieu tous les mercredis et a été élu plus beau marché de France en 2018. Il y a également un marché nocturne :c’est un marché artisanal classé comme l’un des plus beaux de la région PACA. Il a lieu tous les soirs durant la période estivale et se situe sur le port et sur le boulevard d’Estienne Orves. Lors de votre balade dans ce marché, vous verrez des créateurs, artisans, et artistes vendant des produits made in France.

### Les plages de Sanary

**Sanary** vous fera également profiter de ses belles plages : plage du Lido, plage de la Gorguette, plage Dorée, plage de la Baie de Cousse, et plage de Portissol. Elles sont toutes à proximité du centre-ville et sont donc faciles d’accès.

N’hésitez pas à consulter la météo sur **Météo des Plages** pour choisir la meilleure journée pour vous y rendre !


## Quelle est la météo à Sanary-sur-Mer dans le 83 ?

**Sanary-sur-Mer** est une ville où il fait bon vivre. En effet, la météo est très agréable avec un climat chaud et tempéré. La ville bénéficie d’un climat méditerranéen avec un été chaud. Les mois les plus pluvieux sont les mois de septembre, novembre, et octobre. Il est donc idéal de se rendre à **Sanary** pendant les mois d’avril, mai, juin, juillet, août, septembre, et octobre afin de profiter de la ville en toute tranquillité. De mai à août, l’ensoleillement est égal ou supérieur à 10h par jour. Les températures moyennes varient entre 12° en janvier et 28° en juillet. La température de la mer, elle, varie de 13° à 22°.

Pour avoir les **conditions météorologiques de Sanary-sur-Mer** en temps réel, n’hésitez pas à consulter la Météo des Plages qui vous indiquera toutes les informations sur la température de l’air, la température de la mer, le vent, les marées, la houle ainsi que les indices UV à **Sanary et à ses plages alentours**.
