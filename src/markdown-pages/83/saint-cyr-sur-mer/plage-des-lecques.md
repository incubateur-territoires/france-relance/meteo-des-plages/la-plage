---
title: "Plage des Lecques à Saint-Cyr-sur-Mer  | Météo des plages"
description: "La Météo des Plages vous fait découvrir un bijou dans la mer Méditerranée. Bienvenue à la plage des Lecques à Saint-Cyr-sur-Mer !"
---

# La plage des Lecques à Saint-Cyr-sur-Mer

La **plage des Lecques** à **Saint-Cyr-sur-Mer** est la plus longue plage de la station balnéaire avec ses 1,4 kilomètres de long constitué avec alternance de sables et de galets. Située au fond du Golfe d’Amour, elle s’étend du **port de la Madrague** au port de St-Cyr. C’est la zone la plus animée l’été, surtout autour de son port. Un petit bout de paradis que le poète Lamartine qualifiait de ”plus belle baie du monde”.

## Quelles activités faire sur la plage des Lecques à Saint-Cyr-sur-Mer dans le 83 ?

Sur la **plage des Lecques**, vous pouvez profiter pleinement de la douceur du climat méditerranéen, seul, en famille ou entre amis. En plus d’une baignade appréciable et appréciée, vous avez la possibilité de vous exercer à la pratique de plusieurs sports. La commune a mis en place sur cette plage plusieurs installations sportives comme des terrains de beach-volley ou encore un minigolf. Vous avez également la possibilité de pratiquer des activités nautiques (plongée, voile, kayak et pédalo) grâce à la Société Nautique du Golfe des Lecques (SNGL) qui met à votre disposition différents engins nautiques. La pratique de ces sports est soumise aux conditions météorologiques, il vous faudra donc consulter la **météo de la plage des Lecques** avant chaque sortie en mer.

## Quels sont les indicateurs météo moyens sur la plage des Lecques dans le Var en été ?

La **météo sur la plage des Lecques** en période touristique est généralement idyllique. Avec un taux d'ensoleillement extrêmement haut (grâce au Mistral), une température maximale moyenne de 26 °C et une température de l’eau pouvant aller jusqu’à 26 °C en août, les conditions pour une baignade sont presque parfaites. Attention toutefois à la houle, courant et marée pouvant piéger bon nombre de personnes. Il est donc conseillé de regarder la météo de la **plage des Lecques** avant son bain dans la mer.
 
## La baignade est-elle surveillée ?

La surveillance de la baignade **plage des Lecques** s’effectue par des nageurs sauveteurs professionnels en avant-saison (du 30 avril au 26 juin) de 10 h à 18 h. En pleine saison (du 01 juillet au 4 septembre) de 9 h à 19 h et en après-saison (du 10 au 25 septembre 2022) de 10 h à 18 h. Il est impératif de se baigner dans les zones délimitées et nous vous conseillons de consulter la **Météo des Plages** pour éviter toutes mauvaises surprises.

## Équipements sur la plage de Saint-Cyr-sur-Mer en région PACA.

Pour vous garer, il y a plusieurs parkings payants tout au long de la plage. Un accès handicapé est également présent et un dispositif handiplage pour permettre aux personnes à mobilité réduite de pouvoir se baigner en toute sécurité.
À l’extrémité (côté port de la Madrague), vous trouverez de nombreux restaurants et snacks ainsi que des plages privées avec location de matelas, douches et WC.

Patrimoine naturel d’exception, la **baie des Lecques** vous invite à partager son climat méditerranéen et ses vues de cartes postales.
