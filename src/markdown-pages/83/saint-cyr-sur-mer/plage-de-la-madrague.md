---
title: "Plage de la Madrague à Saint-Cyr-sur-Mer  | Météo des plages"
description: "Venez découvrir la plage de la Madrague, plus intimiste et familiale. Baignez-vous en sécurité grâce à la Météo des Plages."
---

# La plage de la Madrague à Saint-Cyr-sur-Mer

La **plage de la Madrague à Saint-Cyr-sur-Mer** est localisée juste à côté du port du même nom. Cette petite plage de sable fin offre une vue sur la Ciotat, et si vous prenez le chemin en bois qui la traverse, vous pourrez rejoindre la **crique du Brameou**.

## Quelles sont les installations sur la plage de la Madrague dans la Var ?

Il existe plusieurs types d’installations pour adultes et enfants sur cette plage.

### Sanitaires et accès handicapé

La **plage de la Madrague** dispose de sanitaires (douches et WC) et d'un accès pour personnes à mobilité réduite. Des fauteuils spécifiques permettant l’accès à l’eau en toute sécurité sont disponibles lors des périodes estivales.

### Un paradis pour enfants ?

Véritable havre de paix, cette plage est parfaite pour les familles et surtout pour les enfants. En effet, vous pouvez trouver une aire de jeux pour enfants en forme de bateau, avec un toboggan.

## Quelle est la météo sur la plage de la Madrague et quels sont les horaires de surveillance de baignade ?

Le climat méditerranéen sur la **plage de la Madrague** rend la baignade ou la bronzette très agréable. La température de l’eau en saison estivale est de 23 °C en moyenne, pour une température extérieure de 26 °C. Attention toutefois au vent qui peut vous incommoder, il est conseillé de consulter la **météo de la plage de la Madrague** avant de s’y rendre. Si jamais la puissance des rafales de vent vous gêne, vous pouvez marcher jusqu’à la crique du Bramaraou qui est à 5 minutes à pied (à l’abri du vent).

## Quels itinéraires pour rejoindre la plage de la Madrague dans le 83 ?

Vous avez plusieurs façons de venir à la **plage de la Madrague**. Route de la Madrague 83270 Saint-Cyr-sur-Mer.

### Se rendre sur la Madrague en voiture

Depuis le centre de Saint-Cyr-sur-Mer, il faut prendre la direction du **port de la Madrague**. Le parking se situe sur la droite en arrivant au port.
Depuis la gare de Toulon, il faut prendre l’autoroute A50 et sortir à la sortie 12 Bandol. Le trajet dure en moyenne 24 minutes pour 23,8 kilomètres.
En partant de la gare de Marseille St Charles, rejoignez l’A50 sur 45 km, il vous faudra 45 min pour rejoindre la **plage de la Madrague** dans le 83.

### Se rendre à la Madrague de manière plus écologique

En bus depuis la gare de Toulon, il faut prendre le 103 jusqu’à Jean Jaurès et ensuite le HY17. Le trajet a une durée de 1 h 36.

À vélo depuis le centre de Toulon, prenez la D559, à une allure modérée, il vous faudra 1h 30 min pour rejoindre la plage. Il vous faudra seulement 10 min depuis le centre de Saint-Cyr-sur-Mer.

À pied, depuis la place Portalis, où se trouve la réplique de la statue de la Liberté, vous mettrez 33 min à faire les 2,6 kilomètres qui vous séparent de la plage.
