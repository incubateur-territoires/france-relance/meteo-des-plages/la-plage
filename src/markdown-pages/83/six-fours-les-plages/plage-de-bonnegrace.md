---
title: "Plage de Bonnegrâce à Six-Fours-les-Plages | Météo des plages"
description: "Baignades, température de l'eau, vitesse du vent  sur la plage de Bonnegrâce. Tout savoir sur la météo de votre plage"
---

# Plage de Bonnegrâce à Six-Fours-les-Plages

La **plage de Bonnegrâce**, aussi appelé Brutal Beach, est située à Six-Fours-les-Plages, le long de la promenade Charles de Gaulle, dans le département du Var, en région Provence-Alpes-Côte d’Azur. C’est une des principales plages dans la périphérie de Toulon. Son adresse est Promenade Charles De Gaulle 83140 Six-Fours-les-Plages, à côté de l’Office de tourisme et proche du port. C'est la plus grande plage de cette ville avec ces 1.2 km de gravillons.

## Comment accéder à la plage de Bonnegrâce à Six-Fours-les-Plages dans le 83 ?

Pour accéder à la plage depuis le centre-ville, rejoignez l'avenue de la mer et prenez la direction de Sanary-sur-Mer. Au rond-point , qui est à l’intersection avec la corniche de Solviou, vous trouverez le début de la **plage de Bonnegrâce**. Si vous venez avec votre véhicule motorisé, le parking public situé à côté du port méditerranée est payant. Pour les personnes non véhiculées, vous avez la possibilité de prendre le bus 70 depuis la gare de Toulon et vous arrêter à l’arrêt Plage de Bonnegrâce. À noter qu’il y a un accès handicapé pour cette plage et que les chiens en laisse sont autorisés sur l'extrémité sud.


## Quels sont les indicateurs moyens en été de la météo de la plage de Bonnegrâce dans le Var ?
En période estivale, la température moyenne extérieure est de 27.6 °C et la vitesse du vent est de 25 km/h sur les dernières années. Pour être plus précis sur la **météo de la plage de Bonnegrâce**, la température de l’eau (moyenne de saison) oscille entre 19.5 °C et 25.4 °C. Pour une baignade en toute sécurité pour les six-fournais et les touristes, la plage de Bonnegrâce est surveillée par une équipe de sauveteurs professionnels et expérimentés. La baignade surveillée pour la saison 2022, débutera le 1er mai tous les week-ends et mercredis de 10 h à 18 h. Elle sera étendue dès le 1er juin à tous les jours de la semaine et du week-end de 9 h à 19 h, et ce, jusqu’au 30 septembre inclus. Il est fortement conseillé de se baigner dans la zone délimitée par les sauveteurs afin de ne pas se faire surprendre par la hauteur de la marée.

### Quelles activités sur la plage de Bonnegrâce dans la périphérie de Toulon ?
La partie de la plage, dite “Brutal Beach”, à l’extrémité vers la Coudoulière, est un endroit prisé par les véliplanchistes et surfeurs qui profitent, grâce au Mistral, de belles vagues. Attention toutefois à bien regarder la **météo de la plage de Bonnegrâce** avant de vous y aventurer et de respecter les consignes de sécurité. Pour les moins téméraires, vous pourrez profiter d’un bain de soleil confortablement installé dans un transat grâce à la location de matelas et de parasols. Des restaurants donnant sur la plage sont aussi présents pour vous permettre de vous alimenter et désaltérer avec une vue magnifique sur la mer méditerranée.
