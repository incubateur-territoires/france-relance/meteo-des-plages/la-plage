---
title: "Plage du Cros à Six-Fours-les-Plages  | Météo des plages"
description: "Venez vous baignez à la plage du Cros en toute sécurité grâce à la météo des plages"
---

# La plage du Cros à Six-Fours-les-Plages

La **plage du Cros à Six-Fours-les-Plages** dans le Var est située à 600 mètres du port du Brusc. Depuis cette plage de sable où la partie Est est plus étroite que la partie Ouest, on a une magnifique vue sur l’île des Embiez. Le point négatif est la proximité de la route qui pourrait nuire à la tranquillité des baignades, et être dangereux pour les enfants.

## La météo sur la plage du Cros en été est-elle idéale ?
En été, la température de la mer Méditerranée sur la **plage du Cros** est comprise entre 19.5 °C et 25.4 °C, pour une température extérieure moyenne de 27.6 °C en après-midi. D’après des données récentes, le vent souffle en moyenne à 25 km/h en cette période de l’année et chasse les nuages pour un taux d'ensoleillement remarquable. C’est donc une météo idéale sur la **plage du Cros** pour une baignade sécurisée.

## Quelles sont les règles et équipements mis en place par la mairie de Six-Fours-les-Plages sur cette plage ?
La **plage du Cros** est non-fumeur, l’amende à laquelle s’exposent les contrevenants peut atteindre un montant de 450 €. Il est également interdit de venir avec des animaux. Cette plage n’a pas d’accès aménagés pour les personnes à mobilité réduite.

**La plage du Cros** est équipée de toilettes et est surveillée durant toute la période estivale. Du 1er au 31 mai 2022 les week-ends et les mercredis uniquement de 10 h à 18 h et à partir du 1er juin jusqu’au 30 septembre inclus tous les jours de 10 h à 19 h. Suivant la dangerosité du vent, des vagues, de la marée et grâce à l’aide de la **météo de la plage du Cros**, les maîtres-nageurs sauveteurs peuvent décider de hisser le drapeau rouge, qui interdit la baignade. Il est fortement recommandé d'obéir à leurs consignes pour éviter de vous/et de les mettre en danger.

### Comment se rendre à la plage du Cros dans le 83 ?
L’adresse est Corniche du Cros 83140 Six-Fours-les-Plages. Si vous venez en voiture par le quai St-Pierre, c’est la deuxième plage sur votre gauche à environ 600 mètres du port du Brusc. Si vous arrivez de la Corniche de la Coudoulière, elle sera à l’intersection avec le Chemin du Cros sur votre droite. Pour vous garer, il y a le parking gratuit du Cros qui accueille 72 véhicules situé au 41 Corniche du Cros 83140 Six-Fours-les-Plages.
Pour ceux qu’il veulent venir en bus, le réseau Mistral (bus de la métropole de Toulon) met à votre disposition le bus 72 qui part de l’hôtel de ville de Six-Fours et s'arrête à l'arrêt Le Cros à 260m de la plage du même nom (10 min de trajet). Pour le même trajet à pied, il vous faudra compter 30 à 45 min.
Depuis la gare de Toulon, le trajet dure 1 h 10 en moyenne, en prenant le bus 70 jusqu’à l’arrêt Gabois et ensuite le 87 direction le Brusc.
