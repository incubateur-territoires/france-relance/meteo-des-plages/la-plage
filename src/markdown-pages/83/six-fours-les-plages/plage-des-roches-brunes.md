---
title: "Plage des Roches Brunes à Six-Fours-les-Plages  | Météo des plages"
description: "Pour une baignade agréable et en toute sécurité dans la mer Méditerranée sur la plage des Roches Brunes, consultez la Météo des Plages."
---

# La plage des Roches Brunes en région Provence-Alpes-Côte d’Azur

La **plage des Roches Brunes** dans la station balnéaire de Six-Fours-les-Plages dans le Var s’étend sur environ de 200 mètres et est un mélange de sable et de gravillons. Elle est située après le Cap nègre en direction du Brusc et en contrebas du Parc de la Méditerranée, juste avant le **port de la Coudoulière**.

## Quelles sont les conditions météorologiques sur la plage des Roches Brunes ?

Sur la période touristique de juin à septembre, la vitesse du vent est en moyenne de 15km/h sur cette plage. La température de l’eau moyenne est de 22 °C et la température de l’air en maximale atteint 30 °C. Pour avoir plus de précisions sur les marées, la houle ou encore les vagues, jour après jour, connectez vous à la **Météo des plages pour la plage des Roches Brunes** dans le 83.

## Quelles activités sur la plage des Roches Brunes à Six-Fours-les-Plages ?

Pour ceux qui ne sont pas satisfait avec seulement des baignades et un bain de soleil, cette plage sera idéale. En effet, cette plage est un spot pour les loisirs nautiques. Vous pourrez ainsi vous exercer à la pratique de la voile, louer des jet-ski et des bateaux, ou encore faire de la plongée. Nous vous invitons à consulter la **météo de la plage des Roches Brunes** pour savoir si ces activités peuvent se faire en toute sécurité.
 
### Équipements et surveillance de la plage des Roches Brunes

Idéale pour les familles, car on a pied assez loin dans la mer, elle l’est aussi pour les personnes en situation de handicap. En effet, elle a le label Handiplage. La **plage des Roches Brunes*** met à disposition un système de baignade adapté aux déficients visuels (Audioplage) qui permet aux personnes mal ou non voyantes de se baigner en toute autonomie et dans des conditions de sécurité optimale. Il y a aussi une rampe d’accès de 25 m, pour personne à mobilité réduite, qui relie les vestiaires, douches et toilettes (tous adaptées aux personnes en situation de handicap). Vous trouverez aussi d'autres équipements comme des fauteuils flottants.
À proximité de la plage, vous trouverez différents commerces dont des restaurants pour vous alimenter.
La **plage des Roches Brunes** est surveillée par des sauveteurs professionnels du 1er juin au 30 septembre inclus, de 9 h à 19 h. Un poste de secours est également présent sur place. Même si la présence des maîtres-nageurs est rassurante, ne prenez aucun risque et avant de vous baigner, consultez la **météo de la plage des Roches Brunes**.

### Adresse et conseils pour accéder à la plage des Roches brunes dans le 83

Son adresse est **Corniche de la Coudoulière 83140 Six-Fours-les-Plages**. Pour vous garer, vous pouvez aller au parking payant au niveau du port de la Coudoulière. En bus, au départ de la gare de Toulon, il vous faudra 1 h 05 de trajet, en prenant le bus 70 et ensuite le 72 à partir de l’arrêt Rayons de Soleil.
