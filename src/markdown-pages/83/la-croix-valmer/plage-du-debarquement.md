---
title: "Plage du Débarquement à La Croix-Valmer  | Météo des plages"
description: "Pour une journée de rêve à la plage du Débarquement à La Croix-Valmer, avec une météo idyllique."
---

# La plage du Débarquement à la Croix-Valmer

La **plage du Débarquement** aussi appelé **de la Douane** est une grande plage de sable dans le prolongement de celle de Cavalaire qui forme avec elle la baie de Cavalaire. C’est la plus animée de la station balnéaire de La Croix-Valmer.

## Pourquoi la plage du Débarquement ?

Historiquement, la plage porta le nom de **“plage de la Douane”**, car elle fut bâtie sur les vestiges de la Villa de Pardigon 2. Mais depuis le 15 août 1944, la plage porte le nom de Débarquement, car c’est ici que la 1re Division Française Libre à débarqué pour libérer la Provence.

## Activités sur la plage du Débarquement dans le 83.

Des activités nautiques sont proposées par le **centre nautique municipal de La Croix-Valmer**, où vous allez pouvoir louer des planches à voile, des catamarans, des kayaks ainsi que des paddles. Le centre nautique propose également des stages de voile pour tous les âges pendant les vacances de Pâques ou d’été. Il est conseillé de consulter la météo de la plage du Débarquement afin de savoir si les conditions météorologiques sont idéales pour faire ces activités.
Vous pourrez aussi faire du pédalo, des bouées tractées et directement sur le sable, vous trouverez des terrains de beach-volley.
La plage étant en face des îles d'Hyères, rendez-vous au ponton pour embarquer sur une navette maritime vers les îles de Porquerolles, Le Levant et de Port Cros.

## Conditions météo et surveillance sur la plage du Débarquement dans le Var.

En période estivale, la plage du Débarquement connaît un climat chaud avec beaucoup de soleil et peu de vent (surtout en août). La température extérieure maximale moyenne est de 27 °C, pour une température de la mer à 23 °C. Pour connaître, jour après jour, les conditions météo sur la plage du Débarquement, ainsi que tous les indicateurs pour une baignade ou une sortie en mer sécurisée, regardez le site de la Météo des Plages.

La **plage du Débarquement**, équipée d’un poste de secours, est surveillée par une équipe de nageurs sauveteurs diplômés du 1er juin au 30 juin de 10 h à 18 h, du 1er juillet au 31 août de 10 h 30 à 18 h 30 et du 1er au 30 septembre de 10 h à 18 h. L’équipe de surveillance s’engage pour la sécurité des baigneurs et la sensibilisation sur les risques dans la mer. Veuillez suivre leurs consignes en complément des prévisions météorologiques annoncées par la **Météo des Plages**.

## Y a-t-il des équipements présents sur la plage du Débarquement en PACA ?

La plage est dotée de sanitaires (douches et toilettes) et d’un service de restauration à proximité. Vous pourrez également louer des matelas pour votre confort. Un parking payant de 228 places voitures et 61 motos est à votre disposition (parking Pardigon).
La **plage du Débarquement** est aussi équipée d’un système d’handiplage pour que les personnes à mobilité réduite puissent se baigner sereinement.
