---
title: "Plage de la Gorguette à Sanary-sur-Mer | Météo des plages"
description: "Passez une belle journée ensoleillée à la plage de la Gorguette à Sanary-sur-Mer ! La Météo des Plages vous informe des conditions météos de la plage."
---

# La plage de la Gorguette à Sanary-sur-Mer


La **plage de la Gorguette** se situe à **Sanary-sur-Mer**, une commune française et une station balnéaire qui se situe dans le département du Var, en Provence-Alpes-Côte d’Azur. C’est une **plage sans tabac** qui est classée en « bonne qualité » depuis 2020. Cette **belle plage de sable blanc** de 200m en pente douce saura vous accueillir pour une journée de détente seul, en famille, ou entre amis. La **Météo des Plages** vous indique en temps réel toutes les informations météorologiques comme la température de l’eau, l’indice UV, le vent, ainsi que les conditions de baignade.

## Pourquoi aller à la plage de la Gorguette à Sanary-sur-Mer ?

La **plage de la Gorguette** à **Sanary sur mer** possède de multiples avantages ! Que ce soit pour une après-midi, une journée, ou même plusieurs jours, la **plage de la Gorguette** saura vous satisfaire. Vous pouvez consulter la **Météo des Plages** pour prévoir vos activités en fonction du temps !

### Une plage réputée pour la qualité de ses eaux de baignade

Vous connaissez peut-être déjà la **plage de la Gorguette** pour la qualité de ses eaux de baignade ! Lors d’un conseil municipal en 2015, la commune de **Sanary** s’est engagée dans la certification de la gestion de la qualité des eaux de baignade, ce qui lui vaut sa belle réputation. 

### Une belle vue sur la baie de Bandol

En effet, la **plage de la Gorguette à Sanary-sur-Mer** offre une magnifique vue sur la **baie de Bandol et l’île de Bendor**. Pour avoir cette vue imprenable, il faut aller en contrebas de l’hôtel de la Farandole sur la **plage de la Gorguette**. 

### Des équipements pratiques

La **plage de la Gorguette** dans le Var possède de nombreux équipements pratiques, qu’on soit seul ou en famille ! Vous trouverez un restaurant à proximité, un parking, des douches, un poste de secours, une surveillance baignade ainsi que la possibilité de louer du matériel.


## Comment se rendre à la plage de la Gorguette dans le Var ?

La plage est accessible par la route cependant elle possède assez peu de places de stationnement à proximité immédiate. Pour trouver des places, vous pouvez aller le long de la **route de Bandol**. De plus, la plage se situe à moins de 5 km de la sortie d’autoroute : assez loin pour être au calme mais pas trop pour rester accessible facilement ! Il y a également un arrêt de transport en commun à moins de 500m et le premier village est à moins de 2km. 

Attention, les animaux ne sont pas acceptés sur la plage de la Gorguette.

## La plage de la Gorguette est-elle surveillée ?

Évidemment ! La baignade de la **plage de la Gorguette** est surveillée en saison estivale, de juin à septembre, entre 10h et 19h, par un poste de secours. De plus, la plage est surveillée en avril et en mai pendant les vacances scolaires et les week-ends.

N’hésitez pas à consulter la **Météo des Plages** pour toute information concernant la température de l’eau, la présence vent, ainsi que les indications concernant la baignade.
