---
title: "Plage de la Baie de Cousse en région PACA | Météo des Plages"
description: "La plage de la Baie de Cousse vous accueille toute l'année à Sanary-sur-Mer ! Profitez d'une journée ensoleillée pour vous baigner."
---

# La plage de la Baie de Cousse en région PACA

La **plage de la Baie de Cousse, également appelée **plage de Beaucours**, est une petite plage tranquille de galets et de sable, qui se situe dans le quartier de Beaucours, au 571 Boulevard de la **Plage de Beaucours à Sanary-sur-Mer**, dans le Var. Plus précisément, elle est située au nord de la pointe de la Cride, dans un beau quartier résidentiel. 

Si vous souhaitez profiter d’une journée ensoleillée sur une petite plage calme avec une très belle eau, rendez-vous sur la **plage de la Baie de Cousse** ! 

## L’eau de la mer de la plage de la Baie de Cousse est-elle de bonne qualité ?

La commune de **Sanary-sur-Mer** s’est engagée dans la certification de la gestion de qualité des eaux de baignade pour toutes ses plages dans le but de garantir les meilleures conditions de baignade aux usagers des plages. La plage est donc classée en « bonne qualité » depuis 2021 !

## La plage de la Baie de Cousse dans le 83 est-elle surveillée ?

Cette plage de galets est surveillée uniquement du 1er juin jusqu’au 30 septembre, de 10h à 19h. De plus, la plage est surveillée en avril et en mai pendant les vacances de toutes les zones, y compris les week-ends et les jours fériés. Il y a aussi la présence d’un poste de secours.

Ne prenez pas de risques face aux conditions météos en consultant la **Météo des Plages** qui vous indiquera toutes les informations nécessaires : indice UV, houle, vent, températures, et marées.

## Quelle route pour accéder à la plage de la Baie de Cousse à Sanary-sur-Mer ?

Si vous arrivez du centre-ville de **Sanary-sur-Mer**, l’accès se fait par l’avenue de Portissol et ensuite par le boulevard Frédéric Mistral. Lorsque vous atteignez la Cride, vous n’aurez plus qu’à prendre à droite sur le boulevard de la **plage de Beaucours** et à vous garer. La plage se situe à environ 300 mètres de ce boulevard et est très facile d’accès. En plein été, il est conseillé de venir jusqu’à la plage à vélo, car les places de parkings se font rare. 

Cette plage a l’avantage d’être située dans une zone à l’écart des grandes routes de Sanary, dans un quartier résidentiel, ce qui fait d’elle une plage très calme et tranquille.

## Quels équipements sont présents sur la plage ?

Sur la **plage de la Baie de Cousse**, vous aurez des douches et des WC. La plage possède également un parking gratuit et est surveillée en saison. Pour les plus dynamiques, vous trouverez un spot de surf où louer des planches de surf et des planches de paddle.
La plage possède un accès pour les personnes en fauteuil roulant.

N’oubliez pas vos masques et vos tubas pour admirer les poissons à travers l’eau claire de la plage de la Baie de Cousse !

N’hésitez pas à consulter la **Météo des Plages** pour prévoir au mieux votre journée et anticiper toutes les informations météos !
