---
title: "Plage du Liouquet à La Ciotat (13) | Météo des Plages"
description: "La plage du Liouquet : une plage peu connue eau turquoise à La Ciotat. Restez informés des conditions météos de la plage avec la Météo des Plages."
---

# La plage du Liouquet à La Ciotat (13)

La **plage du Liouquet** est la dernière **plage de galets des Bouches du Rhône** : elle se situe à l’extrémité est de la **station balnéaire de La Ciotat**. Elle est donc à proximité des plages du Var, située en contrebas de la corniche du Liouquet. C’est une plage non-fumeur ou les chiens ne sont pas admis. Malheureusement, il n’y a pas d’accès handicapés.

## Quelle est la fréquentation de la plage du Liouquet à La Ciotat ?

La fréquentation de cette plage située entre La Ciotat et St-Cyr-sur-Mer est relativement faible. En effet, la **plage du Liouquet** est peu connu des touristes : il a donc peu de monde. De plus, cet endroit vous offrira un magnifique panorama sur la méditerranée. En allant à la plage du Liouquet, profitez d’une des plus belles vues de La Ciotat avec une eau turquoise.

Consultez la **Météo des Plages** pour en savoir plus sur les conditions météo en temps réel de la plage du Liouquet.

## Quels équipements possède la plage du Liouquet ?

La **plage du Liouquet** est une plage accessible à tous. Elle dispose des commodités suivantes : restaurant de plage et toilettes. L’idéal pour faire une pause si vous venez en famille ! Si vous cherchez un logement pour passer vos vacances à côté de la **plage du Liouquet**, l’hôtel Corniche du Liouquet vous accueillera également dans un cadre splendide. Il se situe à moins de 5 kilomètres du centre-ville de La Ciotat, du parcours de golf Frégate et du parc aquatique Aqualand. 

N’hésitez pas à consulter la **météo de la plage du Liouquet** avec la **Météo des Plages** ! Indice UV, températures, vent, marées… Tout vous sera indiqué pour passer la meilleure des journées à la **plage du Liouquet**.

## Comment accéder à la plage du Liouquet à La Ciotat ?

Depuis le centre-ville de La Ciotat, il faut prendre la direction de Saint-Cyr-sur-Mer par la D559. Environ 2 kilomètres avant le port de St-Cyr, prenez la corniche du Liouquet sur la droite. Vous pouvez ensuite vous garer dès que vous trouvez une place sur le bord de la route. Il peut malheureusement être très compliqué de trouver une place en été. Une fois garé, l’accès à la plage se situe à 400 mètres sur la droite. Si vous le souhaitez, il est également possible de se rendre à la **plage du Liouquet** à vélo depuis le centre-ville, ou encore en train.

Pour accéder à la **plage du Liouquet**, il faut emprunter des escaliers à partir de la corniche. L’accès est simple. La partie droite de la plage est interdite, car il y a des risques d’éboulements, cependant, on y trouve quand même des naturistes. Donc seulement la zone située à gauche de l’escalier est autorisée.

## La plage du Liouquet est-elle une plage naturiste ?

Le naturisme est toléré sur la **plage du Liouquet**, mais uniquement dans la partie naturiste qui se situe du côté gauche derrière la roche.
