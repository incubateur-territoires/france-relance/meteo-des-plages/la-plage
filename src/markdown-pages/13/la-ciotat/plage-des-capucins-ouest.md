---
title: "Plage des Capucins à La Ciotat | Météo des Plages"
description: "Vous êtes en vacances à La Ciotat ? Venez profiter de la plage de sable des Capucins, située à proximité du centre-ville."
---

# La plage des Capucins en région PACA

La **plage des Capucins** se situe à **La Ciotat en Bouches-du-Rhône** dans la région **Provence-Alpes-Côte d’Azur**. Cette longue plage de sable s’étend de la place Toche jusqu’au port des Capucins, qui se situe à proximité du Casino. Depuis cette plage, vous aurez une agréable vue sur **l’Ile Verte et sur le bec de l’Aigle** où l’on trouve quelques calanques. Vous reconnaîtrez le rocher du bec de l’Aigle de par sa forme et sa taille. Depuis La Ciotat, vous apercevrez une masse ronde.

## Quelles activités faire lors d’une journée ensoleillée à la plage des Capucins à La Ciotat ?

Pour les plus sportifs, la **plage des Capucins** possède des équipements pour faire du sport ainsi qu’un terrain de beach-volley au niveau de la place Toche. La plage est aussi équipée de douches et de WC.

Vous pouvez également vous rendre sur **l’île Verte**, île que vous pouvez apercevoir depuis la plage, depuis le **vieux port de la Ciotat**. La traversée dure seulement 15 minutes et les traversées ont lieu toutes les heures de 9 h à 18 h en saison estivale.

Si vous souhaitez faire une pause entre deux baignades ou deux séances de bronzage, faites un tour dans les restaurants situés aux alentours. Par exemple, la Brasserie des Capucins pourra vous accueillir : elle propose des plats de qualité avec une vue exceptionnelle. La plage des Capucins étant à proximité du centre-ville de La Ciotat, vous pourrez aussi profiter de tous ses autres restaurants et bars.

La **Météo des Plages** vous indiques les conditions météos de la **plage des Capucins** pour vous aider à prévoir vos activités lors de votre journée plage !

## Comment accéder à la plage des Capucins (13) en voiture ?

La **plage des Capucins** se situant à proximité du centre-ville de La Ciotat, il est très compliqué de trouver une place de parking pour se garer, notamment en été où de nombreux touristes sont présents. Il est recommandé de venir à pied ou à vélo depuis le centre-ville de La Ciotat, ou d’utiliser les transports en commun.
Les bus desservant cette plage sont les bus 200, L069 et L022, et les arrêts les plus proches sont les arrêts Impasse Tivoli et Vallat de Roubaud : ils se situent à seulement 6 minutes de marche de la plage.

Pour choisir la meilleure journée pour vous rendre à la **plage des Capucins**, consultez la **Météo des Plages**.

## La plage des Capucins est-elle surveillée ?

En saison estivale, la **plage des Capucins** est surveillée : elle a un poste de secours disponible dans la partie centrale. Vous pouvez donc venir sans risque en famille avec vos enfants.

## Les chiens sont-ils autorisés à la plage des Capucins ?

La **plage des Capucins** est l’une des rares plages de La Ciotat où vous pourrez emmener vos animaux de compagnie ! En effet, à l’extrémité de la plage, au niveau du terrain de beach-volley, vous trouverez un endroit où les chiens sont autorisés.
