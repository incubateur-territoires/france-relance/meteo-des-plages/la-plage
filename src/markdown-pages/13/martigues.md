---
title: "Martigues, station balnéaire | Météo des Plages"
description: "Profitez des plages de Martigues, la “Venise provençale” grâce à la Météo des Plages"
---

# Martigues, station classée de tourisme

**Martigues** est une ville française en région Provence-Alpes-Côte d’Azur (PACA) dans le département des Bouches-du-Rhône. C’est la quatrième ville en nombre d’habitants (48 574 habitants) du département, et elle est située entre mer et étang, ce qui lui vaut le surnom de **“Venise provençale”**. Depuis 2008, la ville a le label de “Station balnéaire et station de tourisme”. Les amoureux d’une nature provençale authentique seront séduit par **Martigues**, et pourront se balader le long des canaux et/ou de son littoral en découvrant la richesse de son patrimoine.

## Quelles sont les plages de Martigues dans les Bouches-du-Rhône ?

Sable fin, gravillons, criques sauvages, vous en trouverez pour tous les goûts, car **Martigues** dispose de magnifiques plages avec des vues de cartes postales.

### Combien de plages dans la station balnéaire de Martigues et de La Couronne ?

On compte **8 plages sur la commune de Martigues** : plage de Sainte-Croix, plage de La Saulce, plage de Couronne-Vieille, plage du Verdon, plage de Bonnieu, plage de Carro, des Laurons et enfin plage de Ferrières.

La température moyenne de la mer sur les **plages de Martigues** est de 17,5 °C. En été, la moyenne passe à 21,8 °C, avec des pics à 24,5 °C en août. En période estivale, le mistral peut être assez violent avec des rafales pouvant aller jusqu’à 100km/h. Nous vous conseillons de consulter la **météo de votre plage à Martigues** pour avoir tous les indicateurs météorologiques en temps réel.

### Quelles plages sont surveillées à Martigues sur la Côte bleue ?

La **plage du Carro** du 20 juin au 1er juillet de 11h à 19h et du 02 juillet au 28 août de 10h à 19h.
Les **plages de la Saulce, de Sainte-Croix et du Verdon** du 18 juin au 1er juillet de 11h à 19h et du 2 juillet au 28 août de 10h à 19h.
La **plage des Laurons** du 25 juin au 28 août de 11h à 19h
La **plage de Ferrières** du 18 juin au 28 août de 11h à 19h

Les surveillances s’effectuent par des nageurs-sauveteurs professionnels, dans les zones de baignades indiquées par des drapeaux de sécurité. Vous pouvez retrouver la couleur des drapeaux pour savoir si la baignade est autorisée avant de vous déplacer grâce à la **Météo des Plages**.

## Quel est le climat à Martigues dans le 13 ?

Le **climat de Martigues est de type méditérranéen** avec un fort degré d’exposition au vent (mistral). En été, le soleil et le ciel bleu sont la norme, les précipitations quasi-inexistantes et la température maximale moyenne est de 28°C. Un temps idéal pour se baigner dans la Méditerranée, en ayant, par avance, consulter la **météo des plages de Martigues** et de sa côte bleue.

## Quels sont les incontournables de Martigues en région PACA ?

Outre ses belles plages, **Martigues** regorge de merveilles à venir découvrir. 

### Le port de Carro

Petit port de pêche pittoresque, à côté de la **plage du Carro**. Venez le matin découvrir le ballet des bateaux ainsi que son marché à poisson.

### Le musée Ziem

Le musée Ziem datant de 1908, abrite plus de 800 œuvres et objets divers dont les œuvres de Ziem (peintre voyageur). Il est ouvert pendant la saison touristique du mercredi au lundi de 10h à 12h et de 14h à 18h.

### Le sentier du littoral Martégal

Une balade d’une distance de 13 kilomètres où vous pourrez admirer des paysages magnifiques et une diversité écologique préservée. L’accès au sentier peut être interdit suivant les conditions météorologiques.

### Le Miroir aux oiseaux

Classé depuis 1942, ce petit port pittoresque a inspiré de nombreux artistes (Ziem, Delacroix, Corot…) et fait la renommée internationale de la **ville de Martigues**.

## Histoire récente de Martigues

Au XIXe siècle, l’**économie de la ville de Martigues** est exclusivement en rapport avec la pêche. Ce n’est qu’à partir de 1920-1930 que la ville s’industrialise, avec l’installation de l’usine d’oléagineux, et le montage de plusieurs raffineries. Il faudra attendre les événements de mai 68 pour que la ville se développe en véritable site touristique. Au fil des ans, Martigues a su se reconvertir en offrant pendant l’été des activités variées et originales comme le festival summer festiv’halle, des fêtes vénitiennes ou encore des activités de découverte de la pêche.

Prenez le temps de flâner dans les **rues colorées de Martigues** et laissez vous surprendre par des lieux insolites, des statues de Bourvil et Fernandel, par sa plage en plein centre ville… Et n’oubliez pas que pendant votre balade, la **Météo des plages** vous guidera dans tous vos déplacements !
