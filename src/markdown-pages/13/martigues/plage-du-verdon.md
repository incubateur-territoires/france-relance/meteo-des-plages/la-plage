---
title: "Plage du Verdon à Martigues  | Météo des plages"
description: "La plage du Verdon à la Couronne est la plus belle plage du pays de Martigues. Allez vous-y baignez en toute sécurité grâce à la Météo des Plages."
---

# La plage du Verdon à Martigues

La **plage du Verdon** est une belle plage de sable de 300 mètres de long, très appréciée des habitants de la région de Martigues. Baignée par la mer Méditerranée, la plage du Verdon vous attend sur la Côte bleue. Protégée du vent par une grande anse, elle est dotée de tous les équipements pour passer un moment magique.

## Quels sont les équipements sur la plage du Verdon à proximité de Martigues ?

La plage est équipée de douches et de WC pour le confort de tous. Vous trouverez des bars et des restaurants à proximité pour vous rafraîchir et vous restaurer les pieds dans l’eau. Pour admirer cette **magnifique plage et ses alentours** depuis la mer, vous pouvez louer des pédalos. La location est soumise aux conditions météorologiques, consultable sur la **Météo des Plages**. Sur le sable, des terrains de beach-volley sont présents pour vous amuser en famille ou entre amis.
La **plage du Verdon** possède également des aides à la baignade pour les personnes en situation de handicap comme le tiralo ou l’Hippocampe qui est un fauteuil tout terrain et flottant. Ces fauteuils sont disponibles sur inscription au minimum 24h à l’avance en téléphonant au 04 86 64 19 91.

## La météo sur la plage du Verdon sur la Côte Bleue.

En moyenne, en été, la météo sur la plage est très clémente. Entre ciel bleu sans nuage, température extérieure et de l’eau idéale pour se baigner, et protection du vent par une grande anse, on comprend pourquoi la plage du Verdon est la plage favorite des habitants des alentours. Pour savoir précisément et en temps réel les conditions sur cette plage, il vous faut aller sur le site de la **météo de la plage du Verdon**.

## La surveillance de la baignade sur la plage du Verdon dans les Bouches-du-Rhône.

La plage est surveillée en saison estivale les week-ends des 4/5/6 et 11/12 juin 2022 de 11 h à 19 h. Ainsi que du 18 juin au 1er juillet de 11 h à 19 h et enfin du 2 juillet au 28 août de 10 h à 19 h. Pendant la saison touristique, mais également en septembre lorsque les plages ne sont plus surveillées, il est primordial de consulter la **météo de la plage du Verdon**, afin de ne prendre aucun risque lors de vos baignades.

## Accès à la plage du Verdon dans le 13

### De Martigues à la plage du Verdon

En voiture, du centre de Martigues, il faut prendre la départementale 49 sur 12,6 kilomètres. Il vous faudra environ 20 minutes pour arriver à la **plage du Verdon** suivant les conditions sur la route.

### Où se garer pour aller à la plage du Verdon ?

Un parking est à quelques mètres seulement de la plage (parking du verdon et de la couronne). Le stationnement peut cependant être difficile l’après-midi en haute saison et particulièrement le week-end. Il est conseillé de venir dans la matinée pour trouver des places libres.
