---
title: "Plage de Ferrières à Martigues  | Météo des plages"
description: "Vous ne vouliez pas choisir entre plage et visite du centre ville ? Faites les deux dans la même journée avec la plage de la Ferrières à Martigues."
---

# La plage de Ferrières à Martigues

La **plage de Ferrières à Martigues** est située dans le centre-ville, au bord de l’étang de Berre. La **plage de sable** est certifiée “Eaux de Baignade”, la baignade y est autorisée depuis le 1er juin 2017, reconnaissance des actions entreprises par la ville pour la gestion d’un site de baignade. Grâce à sa proximité avec le centre-ville, vous pouvez très bien associer une journée visite du centre historique et une journée à la plage.

## Météo et surveillance sur la plage de Ferrières à Martigues

La plage, sous le climat de type tempéré chaud, est surveillée en période estivale.

## Météo sur la plage de Ferrières

Le climat étant de type méditerranéen, le soleil et la chaleur sont généralement présents sur la plage en période touristique. La température de l’eau moyenne est de 23 °C en saison et les rafales de vent de 30km/h. Les indicateurs météorologiques peuvent évoluer du jour au lendemain, il est donc nécessaire de consulter la météo de la **plage de Ferrières à Martigues**, pour savoir si les conditions sur le sable et dans l’eau pour une baignade sont réunies.

## Surveillance de la plage de Ferrières en centre-ville

La plage est surveillée et la baignade balisée de juin à fin août. Dans le détail, tous les week-ends au mois de juin et tous les jours de juillet et d’août de 11 h à 19 h. Cette plage de centre-ville est équipée d’un poste de secours, tenu par des sauveteurs diplômés. Il est obligatoire de respecter la signalisation mise en place par ces derniers, et vous pouvez aussi regarder la **météo de la plage de Ferrières** pour une baignade en toute sécurité.

## Que pouvons-nous faire sur et autour de la plage de Ferrière dans les Bouches-du-Rhône ?

La plage participe à la dynamisation du centre-ville et à la démarche de la municipalité d'inscrire l’étang de Berre au patrimoine mondial de l’Unesco.

### Installation sur la plage

La plage est dotée de sanitaires (douches et WC) pour pouvoir se rincer avant ou après une baignade. Il y a une aire de jeux pour enfants et une fontaine dite “fontaine sèche”. Tous les jours de mai à septembre, au petit matin, le sable est nettoyé pour garantir le confort des Martégaux et touristes. Vous trouverez également trois paillotes pour vous rafraîchir et restaurer, et des animations musicales (journée ou soirée).
De juillet à août, des ateliers et activités sont proposés tous les mardis : ateliers jonglage, réveil musculaire, ateliers lecture, échecs... Ainsi que le vendredi : ateliers numériques.

### Visite de la ville de Martigues après une baignade

Entre deux baignades ou bains de soleil sur le sable, vous aurez le loisir de visiter la **ville de Martigues**, entre canaux et rues du centre historique. N'oubliez pas de vous couvrir la tête, car le soleil peut taper très fort dans cette région. Consulter la **Météo des Plages** pour toute information.
