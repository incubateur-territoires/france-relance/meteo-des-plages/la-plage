---
title: "Plage de Sainte-Croix à Martigues  | Météo des plages"
description: "Durant vos vacances à Martigues, venez profiter du beau temps sur la plage de Sainte-Croix !"
---

# La plage de Sainte-Croix à proximité de Martigues

La **plage de Sainte-Croix**, dans la station balnéaire de La Couronne, fait partie des **plus belles plages de Martigues**. Elle est séparée seulement par un rocher de la plage de la Saulce (lieu de tournage de Camping paradis). C’est une **plage de sable fin** de 100 mètres de longueur pour 40 mètres de largeur. La plage de Sainte-Croix offre un cadre idyllique avec ses eaux turquoises et sa plage entourée de pinèdes, qui offrent une ombre appréciable en été.

## Surveillance des baignades sur la plage de Sainte-Croix dans les Bouches-du-Rhône

La surveillance des baignades en période estivale est effectuée par des nageurs-sauveteurs diplômés. Ils ont la mission d’autoriser ou non la baignade et de délimiter une zone de surveillance en adéquation avec le niveau de dangerosité de la mer. Pour savoir la force du courant et de la houle et les conditions de baignade, vous pouvez consulter la **météo de la plage de Sainte-Croix**.
La **plage de Sainte-Croix** est surveillée :
Les week-ends début juin de 11 h à 19 h.
Du 18 juin au 1er juillet 2022 de 11 h à 19 h.
Du 2 juillet au 28 août de 10 h à 19 h.
Pour les périodes non surveillées, veuillez regarder la **Météo des plages** pour vous baigner en toute sécurité.

## Que faire sur la plage de Sainte-Croix dans le 13 ?

Dans un premier temps, vous pouvez apprécier tout simplement de vous allonger sur un sable fin et de vous rafraîchir dans la mer de couleur turquoise. Ensuite, la **plage de Sainte-Croix** propose une activité pédalo pour admirer la Côte Bleue depuis la Méditerranée (sous condition d’une bonne météo sur votre plage de Ste-Croix). Vous pourrez continuer par déjeuner dans l’un des restaurants à proximité, avant d’aller voir la chapelle blanche Ste-Croix à l’extrémité Est de la plage.
Pour les plus courageux, vous pouvez entreprendre une balade sur le sentier du Littoral, pour découvrir la beauté de la Côte Bleue ainsi que d’autres plages et criques.

## Quels moyens de transport pour se rendre à la plage de Sainte-Croix en PACA ?

Depuis la gare de Martigues, vous avez trois moyens de transport principaux.

### S’y rendre en voiture

Via la D49, vous mettrez 16 minutes en moyenne, suivant les conditions de circulation pour un trajet de 11 km. Pour vous garer à la plage, il y a un grand parking payant (450 places).

### S’y rendre en train

Prendre le TER à la gare de Martigues direction Marseille St-Charles pour 7 arrêts. À la gare de La Couronne-Carro, il vous reste 2,2 km à pied. (environ 25 min)

### S’y rendre à vélo

Via le chemin des Olives, il vous faudra 40 minutes pour venir à bout des 10 km séparant Martigues à la plage de Sainte-Croix.

Comme toutes les belles plages de la côte méditerranéenne, la **plage de Sainte-Croix** est très prisée l’été ! Prévoyez d’arriver tôt afin de pouvoir profiter du calme matinal.
