---
title: "Plage de Carro à Martigues  | Météo des plages"
description: "Passez une belle journée ensoleillée à la plage de la Carro à proximité de Martigues ! La Météo des Plages vous informe des conditions météos de la plage."
---

# La plage de Carro à Martigues

La **plage de Carro à Martigues** est située juste à côté du port de pêche de Carro. Elle n’est pas très grande (80 mètres) et est constituée de sable et de galets. Elle est sur la Côte bleue, juste à la sortie de La Couronne.


## Activités à la plage de Carro à proximité de Martigues

Les fonds marins, d’une qualité exceptionnelle, permettent la pratique du snorkeling et de la plongée sous-marine comme beaucoup de coins de la Côte Bleue. Ici, grâce à la carrière antique de Baou Tailla, les plongeurs expérimentés peuvent s’exercer à la plongée de bord et passer par un siphon qui les envoie directement en profondeur.

Sur la pointe du port, vous pouvez retrouver le spot de surf des Arnettes, un des meilleurs de la région, qui grâce à sa situation géographique, reçoit toutes les houles et les vents. Vous pourrez aussi faire d’autres sports de glisse comme du bodyboard ou du paddle. Pour connaître les prévisions météorologiques et de vagues, regardez la **Météo des Plages**.

## Météo et surveillance sur la plage Carro dans le 13

La **plage de Carro** est dans une anse protégée des vents, ce qui n’est pas négligeable dans une région touchée par de fortes rafales de vent.

### Quelle météo en été sur la plage ?

Comme dans toute la région, le climat est de type méditerranéen. Ce qui signifie, en été, un soleil au beau fixe, une chaleur intense et très peu de précipitation. La température de l’eau est en moyenne de 21 °C. Pour savoir la vitesse du vent, la houle, la température de l’air et de l’eau… Connectez-vous sur la météo de votre **plage de Carro**. Vous aurez toutes ces informations et bien plus encore.

### La baignade sur la plage de Carro est-elle surveillée ?

La **plage de Carro** est uniquement surveillée en période touristique. Du 20 juin au 1er juillet 2022 de 11 h à 19 h et du 2 juillet au 28 août de 10 h à 19 h. Les maîtres nageurs vous indiquent s'il est possible ou non de se baigner dans la mer et si le cas échéant, c’est possible, délimitent une zone sécurisée de baignade. Avant de vous déplacer, vous pouvez connaître les conditions de baignade sur la **plage de Carro** grâce à la **Météo des Plages**.

## Comment se rendre sur la plage de Carro dans les Bouches-du-Rhône ?

La plage se trouve avenue rené Fouque 13500 Carro-La Couronne. Nos conseils de trajet en suivant sont d’un départ du centre de Martigues.

### Venir en voiture

Le trajet le plus rapide (18 minutes) pour aller sur la **plage de Carro** depuis le centre de Martigues est de prendre la D49 sur 12,5 kilomètres. À l’arrivée, vous pourrez vous garer sur l’un des parkings aménagés. Ensuite, l’accès depuis la route se fait par des chemins en pente douce bétonnés.

### Venir à vélo

À vélo, c’est un trajet de 52 minutes qui vous attend pour effectuer les 13,2 kilomètres qui vous séparent de la **plage de Carro**.
