---
title: "Plage des Laurons à Martigues  | Météo des plages"
description: "Passez une journée en famille à la plage des Laurons à 10 min en voiture de Martigues !"
---

# La plage des Laurons à Martigues

La **plage des Laurons** dans la **station balnéaire de Martigues** est une plage de 100 mètres de long composée en partie de sable, de galets et de pelouse. Elle est située à côté du port des Laurons, sur la route du Sentier du Littoral. À noter la présence d’une centrale thermique EDF à 300 mètres qui gâche le paysage et peut dégager une odeur désagréable.

## Comment venir à la plage des Laurons dans le 13 ?

À partir du centre-ville de Martigues, vous avez différents moyens pour venir à la **plage des Laurons** située Route de Ponteau 13117 Martigues.

### Aller en voiture à la plage des Laurons

Le plus rapide en voiture est d’emprunter la départementale 9 (D9). Le trajet dure en moyenne 11 minutes pour 8,4 kilomètres. Un parking gratuit est situé à droite de la plage.

### Aller en bus à la plage des Laurons

De l’arrêt Hôtel de Ville prenez le bus Ulysse numéro 23, pour un trajet de 33 minutes (30 arrêts), jusqu’à l’arrêt Pieds Eau 1. Il vous faudra ensuite marcher 150 mètres pour atteindre la plage.

### Aller à pied ou vélo à la plage des Laurons

Pour les plus sportifs ou courageux, vous pouvez faire les 9 km qui vous séparent de la plage en 30 minutes à vélo ou en 1 h 45 à pied. Regarder la **météo des plages** avant de partir et n’oubliez de vous désaltérer.

## La plage des Laurons dans les Bouches-du-Rhône est-elle surveillée ?

La surveillance de la **plage des Laurons** s’effectue du 25 juin au 28 août 2022 de 11 h à 19 h, tous les jours de la semaine et du week-end. Les maîtres nageurs sauveteurs balisent une zone de baignade surveillée pour la sécurité de tous. Il est également fortement conseillé de regarder la météo de la plage des Laurons pour être sûr de ne prendre aucun risque lors de vos baignades.

## Quel est la température de l’eau sur la plage des Laurons en PACA ?

Selon le courant et le vent, la température de la mer en été est comprise entre 17 °C et 24 °C sur la **plage des Laurons à Martigues**. Pour connaître la puissance et la direction du vent, la houle, la température en temps réel de l’eau, la dangerosité de la baignade, veuillez consulter la **météo de votre plage**.

## Aménagements à la plage des Laurons à Martigues

Une aire de pique-nique a été aménagée avec bancs, ombre et tables qui permettent de passer une journée en famille sans bouger de la plage. Il y a également des douches et WC pour le confort des vacanciers. Une aire de jeux pour enfants et un terrain omnisports complètent les aménagements.
Les animaux ne sont pas acceptés sur la plage sauf les chien-guides d’aveugles.

La **plage des Laurons** a le charme d’une plage isolée surtout en fin de journée avec le coucher de soleil. Pour connaître l’horaire du coucher de soleil, rejoignez la communauté **météo des plages**.
