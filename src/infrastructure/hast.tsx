import React, { createElement, Fragment } from 'react';
import { unified } from 'unified';
import rehypeReact from 'rehype-react';
import { Typography, TypographyProps } from '@mui/material';

const options = {
  createElement,
  components: {
    p: (props: TypographyProps) => <Typography variant="body1" {...props} />,
    h1: (props: TypographyProps) => <Typography variant="h1" {...props} />,
    h2: (props: TypographyProps) => <Typography variant="h2" {...props} />,
    h3: (props: TypographyProps) => <Typography variant="h3" {...props} />,
  },
  Fragment,
};

// @ts-ignore
export const processor = unified().use(rehypeReact, options);

export const renderHast = processor.stringify;

export const createHastRootNode = (children: [], type = 'root') => {
  return {
    type,
    children,
    data: {
      quirksMode: false,
    },
  };
};

export function splitHastUntil<ItemType>(
  hast?: { type: string; children: ItemType[] },
  callback: (e: ItemType) => boolean
) {
  let firstHalf, secondHalf;
  if (hast && hast.children) {
    for (let i = 0; i < hast.children.length; i++) {
      const check = callback(hast.children[i]);
      if (check) {
        firstHalf = createHastRootNode(hast.children.slice(0, i), hast.type);
        secondHalf = createHastRootNode(hast.children.slice(i), hast.type);
        break;
      }
    }
  }
  return [firstHalf, secondHalf];
}
