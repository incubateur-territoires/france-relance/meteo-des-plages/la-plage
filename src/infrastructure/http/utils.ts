export const toJson = (response: Response) => response.json();

export const toConsole = <T>(response: T): T => {
  console.log(response);
  return response;
};
