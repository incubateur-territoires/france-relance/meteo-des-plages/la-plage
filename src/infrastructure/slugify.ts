import simovSlugify from 'slugify';

export const slugify = (string: string) => simovSlugify(string, { strict: true, lower: true });
