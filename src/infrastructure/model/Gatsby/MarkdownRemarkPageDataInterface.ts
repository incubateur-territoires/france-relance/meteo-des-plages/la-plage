import { PageMetadata } from '../../../model/Page/PageCustomizationInterface';

export interface MarkdownRemarkPageDataInterface {
  html: string;
  htmlAst: Record<string, unknown>;
  frontmatter: PageMetadata;
}
