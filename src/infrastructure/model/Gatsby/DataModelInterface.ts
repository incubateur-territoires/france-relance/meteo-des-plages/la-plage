export interface DataModelInterface {
  id: unknown;
  [key: string]: unknown;
}
