import { VilleInterface } from '../../../model/VilleInterface';
import { PlageInterface } from '../../../model/PlageInterface';

export type PartialPlageWithPath = Pick<PlageInterface, 'id' | 'nom'> & {
  path: string;
  ville: Pick<VilleInterface, 'nom' | 'departement'>;
};

export interface VilleNodeInterface extends VilleInterface {
  plages: PartialPlageWithPath[];
  path: string;
}
