export interface LieuDtoInterface {
  nom: string;
  id_lieu: number;
  latitude: number;
  longitude: number;
  ville: string;
  departement: number;
  telephone?: string | number;
  email?: string;
  web?: string;
  tag_lieu_cat?: string; // "nature, plage, ",
  codepostal: number;
}
