export interface VilleDtoInterface {
  ville_latitude: number;
  ville_longitude: number;
  ville: string;
  departement: number;
  codepostal: number;
}
