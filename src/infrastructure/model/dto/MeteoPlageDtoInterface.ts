import { TypeDeDrapeau } from '../../../model/Meteo/Drapeau/TypeDeDrapeau';
import { EtatMer } from '../../../model/Meteo/EtatMer';
import { QualiteEau } from '../../../model/Meteo/QualiteEau';

export interface MeteoPlageDtoInterface {
  // LieuDtoInterface
  nom: string;
  latitude: number;
  longitude: number;
  ville: number;
  departement: number;
  codepostal: number;
  web: null;
  email: null;

  // MeteoPlageDtoInterface
  id_lieu: string;
  met_icon: 803;
  temperatureAir: 25; // ° Celsius
  met_vent: 23;
  met_vent_dir: 321;
  dateMaj: string;
  /**
   * - 0 > pas de drapeau / pas de donnée
   * - 1 > vert (ex: https://www.frequence-sud.fr/img/navigation/ux/meteo/flag2_1.png)
   * - 2 > jaune
   * - 3 > rouge
   * - 4 > violet / pollution
   */
  drapeau: 0 | TypeDeDrapeau;
  temperatureEau: number;
  etatMer?: EtatMer | 'non disponible';
  alerte?: string;
  commentaire: string;
  qualite: QualiteEau;
  statut: 'réel' | 'dégradé';
}
