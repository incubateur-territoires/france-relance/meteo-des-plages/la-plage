export enum FrequenceSudAlerteType {
  meduse = 'meduse',
  meduseUtil = '-meduse util-',
  raie = 'raie',
  courant = 'courant',
  meteo = 'meteo',
  inondation = 'inondation',
  faible = 'faible',
  ferme = 'ferme',
}
