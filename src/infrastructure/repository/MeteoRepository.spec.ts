import { MeteoRepository } from './MeteoRepository';
import { MeteoPlageAlerteType } from '../../model/Meteo/MeteoPlageAlerteType';
import { FrequenceSudAlerteType } from '../model/dto/FrequenceSudAlerteType';

describe('MeteoPlageRepository', () => {
  describe('fromFrequenceSudAlerteTypeToAlerteType', () => {
    it('peut traduire une alert fréquence sud', () => {
      expect(MeteoRepository.fromFrequenceSudAlerteTypeToAlerteType(FrequenceSudAlerteType.meduseUtil)).toEqual(
        MeteoPlageAlerteType.PresenceMeduses
      );
    });
  });

  describe('extractAlertes', () => {
    it('peut extraire les alertes', () => {
      expect(MeteoRepository.extractAlertes(' -meduse util-')).toEqual([MeteoPlageAlerteType.PresenceMeduses]);
      expect(MeteoRepository.extractAlertes('')).toEqual([]);
      expect(MeteoRepository.extractAlertes(' meduse, raie')).toEqual([
        MeteoPlageAlerteType.PresenceMeduses,
        MeteoPlageAlerteType.PresenceRaies,
      ]);
    });
  });
});
