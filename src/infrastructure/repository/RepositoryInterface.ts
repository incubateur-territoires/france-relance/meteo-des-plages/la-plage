export interface RepositoryInterface<E> {
  findAll: () => Promise<E[]>;
}
