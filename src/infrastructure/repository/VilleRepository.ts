import { RepositoryInterface } from './RepositoryInterface';
import { FrequenceSudClient } from '../FrequenceSudClient';
import { VilleDtoInterface } from '../model/dto/VilleDtoInterface';
import { Ville } from '../../model/Ville';

export class VilleRepository implements RepositoryInterface<Ville> {
  private client: FrequenceSudClient;
  private villes: Ville[] = [];

  constructor(client: FrequenceSudClient) {
    this.client = client;
  }

  async findAll(): Promise<Ville[]> {
    await this.fetchAllIfNotAlready();
    return this.villes;
  }

  async findOneByNom(nom: string) {
    await this.fetchAllIfNotAlready();
    return this.villes.find((ville) => {
      return ville.getNormalizedNom() === Ville.normalizeNom(nom);
    });
  }

  async fetchAllIfNotAlready() {
    if (this.villes.length === 0) {
      await this.fetchAll();
    }
  }

  private async fetchAll(): Promise<void> {
    this.villes = await this.client.fetchVilles().then((villeDtos) => villeDtos.map(this.fromDto));
  }

  fromDto({ ville_latitude, ville_longitude, ville: nom, departement, codepostal }: VilleDtoInterface) {
    return new Ville({
      id: Ville.computeId({ departement: departement.toString(), nom }),
      coordinates: {
        latitude: ville_latitude,
        longitude: ville_longitude,
      },
      nom,
      departement: departement.toString(),
      codePostal: codepostal,
    });
  }
}
