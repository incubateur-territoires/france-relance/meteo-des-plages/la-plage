import { RepositoryInterface } from './RepositoryInterface';
import { FrequenceSudClient } from '../FrequenceSudClient';
import { LieuDtoInterface } from '../model/dto/LieuDtoInterface';
import { VilleRepository } from './VilleRepository';
import { Plage } from '../../model/Plage';
import { MeteoRepository } from './MeteoRepository';

const asyncFilter = async <Entry>(entries: Entry[], predicate: (entry: Entry) => Promise<boolean>) => {
  const results = await Promise.all(entries.map(predicate));

  return entries.filter((_v, index) => results[index]);
};

export class PlageRepository implements RepositoryInterface<Plage> {
  private client: FrequenceSudClient;
  private villeRepository: VilleRepository;
  private meteoRepository: MeteoRepository;
  private plages: Plage[];

  constructor(client: FrequenceSudClient, villeRepository: VilleRepository, meteoRepository: MeteoRepository) {
    this.client = client;
    this.villeRepository = villeRepository;
    this.meteoRepository = meteoRepository;
    this.plages = [];
  }

  async findAll(): Promise<Plage[]> {
    await this.fetchAllIfNotAlready();
    return this.plages;
  }

  async fetchAllIfNotAlready() {
    if (this.plages.length === 0) {
      await this.fetchAll();
    }
  }

  async findOneById(id: string) {
    await this.fetchAllIfNotAlready();
    return this.plages.find((plage) => {
      return plage.id === id;
    });
  }

  private async fetchAll(): Promise<void> {
    await this.villeRepository.fetchAllIfNotAlready();
    await this.meteoRepository.fetchAllIfNotAlready();
    const lieuDtos = await this.client.fetchPlages();
    const lieuDtosWithVille = await asyncFilter<LieuDtoInterface>(lieuDtos, async ({ ville: villeNom, nom }) => {
      const ville = await this.villeRepository.findOneByNom(villeNom);
      if (!ville) {
        console.info(`Filtering out plage "${nom}" because "${villeNom}" couldn't be found.`);
      }
      return !!ville;
    });

    this.plages = await Promise.all(
      lieuDtosWithVille
        .filter(({ nom, latitude }) => {
          if (!latitude) {
            console.info(`Filtering out plage "${nom}" because it has no coordinates.`);
            return false;
          }

          return true;
        })
        .map(this.fromDto.bind(this))
    );
  }

  async fromDto({ latitude, longitude, ville: villeNom, ...lieuDto }: LieuDtoInterface): Promise<Plage> {
    const ville = await this.villeRepository.findOneByNom(villeNom);
    if (!ville) {
      throw new Error(`Impossible de trouver une ville portant le nom "${villeNom}".`);
    }

    const meteoPlage = await this.meteoRepository.findOneByPlageId(lieuDto.id_lieu.toString());
    if (!meteoPlage) {
      console.warn(`Impossible de trouver une météo pour la plage "${lieuDto.nom}" portant l'id "${lieuDto.id_lieu}".`);
    }

    return new Plage({
      id: Plage.computeId(lieuDto.nom),
      frequenceSudId: lieuDto.id_lieu,
      nom: lieuDto.nom,
      ville: { ...ville }, // /!\ Gatsby data layer won't accept a class here so we're just extracting properties.
      villeId: ville.id,
      email: lieuDto.email,
      telephone: typeof lieuDto.telephone === 'number' ? `0${lieuDto.telephone}` : lieuDto.telephone,
      coordinates: {
        latitude,
        longitude,
      },
      siteWeb: lieuDto.web,
      tags: lieuDto.tag_lieu_cat?.split(', ').filter((tag) => tag) || [],
      meteo: meteoPlage && { ...meteoPlage },
    });
  }
}
