import { RepositoryInterface } from './RepositoryInterface';
import { FrequenceSudClient } from '../FrequenceSudClient';
import { Meteo } from '../../model/Meteo/Meteo';
import { MeteoPlageDtoInterface } from '../model/dto/MeteoPlageDtoInterface';
import { TypeDeDrapeau } from '../../model/Meteo/Drapeau/TypeDeDrapeau';
import { MeteoSourceType } from '../../model/Meteo/MeteoSourceType';
import { MeteoPlageAlerteType } from '../../model/Meteo/MeteoPlageAlerteType';
import { FrequenceSudAlerteType } from '../model/dto/FrequenceSudAlerteType';

export class MeteoRepository implements RepositoryInterface<Meteo> {
  private client: FrequenceSudClient;
  private meteos: Meteo[];

  constructor(client: FrequenceSudClient) {
    this.client = client;
    this.meteos = [];
  }

  async findAll(): Promise<Meteo[]> {
    await this.fetchAllIfNotAlready();

    return this.meteos;
  }

  async findOneByPlageId(plageId: string) {
    await this.fetchAllIfNotAlready();
    return this.meteos.find((meteo) => meteo.plageId === plageId);
  }

  async fetchAllIfNotAlready() {
    if (this.meteos.length === 0) {
      await this.fetchAll();
    }
  }

  private async fetchAll(): Promise<void> {
    const meteoDtos = await this.client.fetchMeteoPlages();
    this.meteos = await Promise.all(meteoDtos.map(this.fromDto.bind(this)));
  }

  async fromDto({
    id_lieu,
    commentaire,
    alerte,
    statut,
    drapeau,
    temperatureEau,
    etatMer,
    qualite,
    temperatureAir,
    dateMaj,
    met_icon,
    met_vent,
    met_vent_dir,
  }: MeteoPlageDtoInterface): Promise<Meteo> {
    return new Meteo({
      plageId: id_lieu.toString(),
      commentaire: commentaire,
      // @ts-ignore
      alertes: MeteoRepository.extractAlertes(alerte),
      merEtat: MeteoRepository.extractMerEtat(etatMer),
      sourceType: MeteoRepository.extractSourceType(statut),
      temperature: temperatureAir,
      eau: {
        qualite,
        temperature: MeteoRepository.parseFloat(temperatureEau),
      },
      derniereMiseAJour: MeteoRepository.parseDate(dateMaj),
      openWeatherMapCode: met_icon,
      typeDeDrapeau: MeteoRepository.fromFrequenceSudDrapeauToTypeDeDrapeau(drapeau),
      vent: {
        direction: met_vent_dir,
        vitesse: met_vent,
      },
    });
  }

  static parseDate(dateString: string) {
    const b = dateString
      .replace(' ', 'T')
      .split(/\D/)
      .map((b) => parseInt(b, 10));

    return new Date(b[0], b[1] - 1, b[2], b[3], b[4], b[5]);
  }

  static parseFloat(n?: string | number) {
    if (!n) {
      return;
    } else if (typeof n === 'number') {
      return n;
    }

    return parseFloat(n);
  }

  private static extractMerEtat(etatMer: MeteoPlageDtoInterface['etatMer']) {
    return etatMer === 'non disponible' ? undefined : etatMer;
  }

  static extractAlertes(alerte?: MeteoPlageDtoInterface['alerte']) {
    return !alerte
      ? []
      : alerte
          .split(',')
          .map((a) => MeteoRepository.fromFrequenceSudAlerteTypeToAlerteType(a.trim() as FrequenceSudAlerteType))
          .filter((a) => !!a);
  }

  private static extractSourceType(statut: MeteoPlageDtoInterface['statut']) {
    return statut === 'réel' ? MeteoSourceType.Manuel : MeteoSourceType.Automatique;
  }

  private static fromFrequenceSudDrapeauToTypeDeDrapeau(drapeau: 0 | TypeDeDrapeau) {
    return drapeau === 0 ? undefined : drapeau;
  }

  static fromFrequenceSudAlerteTypeToAlerteType(alertType: FrequenceSudAlerteType): MeteoPlageAlerteType | undefined {
    switch (alertType) {
      case FrequenceSudAlerteType.courant:
        return MeteoPlageAlerteType.CourantsDangereux;
      case FrequenceSudAlerteType.meteo:
        return MeteoPlageAlerteType.DegradationMeteo;
      case FrequenceSudAlerteType.faible:
        return MeteoPlageAlerteType.NiveauEauFaible;
      case FrequenceSudAlerteType.meduse:
      case FrequenceSudAlerteType.meduseUtil:
        return MeteoPlageAlerteType.PresenceMeduses;
      case FrequenceSudAlerteType.ferme:
        return MeteoPlageAlerteType.PlageFermeAuPublic;
      case FrequenceSudAlerteType.inondation:
        return MeteoPlageAlerteType.Inondations;
      case FrequenceSudAlerteType.raie:
        return MeteoPlageAlerteType.PresenceRaies;
      default:
        return;
    }
  }
}
