import fetch from 'node-fetch';
import { toJson } from './http/utils';
import { LieuDtoInterface } from './model/dto/LieuDtoInterface';
import { VilleDtoInterface } from './model/dto/VilleDtoInterface';
import { MeteoPlageDtoInterface } from './model/dto/MeteoPlageDtoInterface';

export class FrequenceSudClient {
  public baseUrl: string;

  constructor(baseUrl: string) {
    this.baseUrl = baseUrl;
  }

  public fetchPlages() {
    return this.fetch('/liste_lieux.php')
      .then(toJson)
      .then((jsonResponse) => jsonResponse.data as LieuDtoInterface[]);
  }

  public fetchMeteoPlages() {
    return this.fetch('/flux.php')
      .then(toJson)
      .then((jsonResponse) => jsonResponse as MeteoPlageDtoInterface[]);
  }

  public fetchVilles() {
    return this.fetch('/liste_villes.php')
      .then(toJson)
      .then((jsonResponse) => jsonResponse.data as VilleDtoInterface[]);
  }

  private fetch(endpoint: string, init?: RequestInit): Promise<Response> {
    return fetch(this.getFullEndpointURL(endpoint), { method: 'GET', ...init });
  }

  private getFullEndpointURL(endpoint: string) {
    return `${this.baseUrl}${endpoint}`;
  }
}
