import type { GatsbyNode } from 'gatsby';
import { FileSystemNode } from 'gatsby-source-filesystem';
import path from 'path';
import { PlageRepository } from './src/infrastructure/repository/PlageRepository';
import { VilleRepository } from './src/infrastructure/repository/VilleRepository';
import { FrequenceSudClient } from './src/infrastructure/FrequenceSudClient';
import { Plage } from './src/model/Plage';
import { Ville } from './src/model/Ville';
import { SourceNodesArgs } from 'gatsby';
import { MeteoRepository } from './src/infrastructure/repository/MeteoRepository';
import { DataModelInterface } from './src/infrastructure/model/Gatsby/DataModelInterface';

const createCreateNodeFromDataModel =
  ({ actions, createContentDigest }: SourceNodesArgs) =>
  (data: DataModelInterface, type: string) => {
    const { createNode } = actions;
    return createNode({
      ...data,
      parent: null,
      children: [],
      internal: {
        type,
        content: JSON.stringify(data),
        contentDigest: createContentDigest(data),
      },
    });
  };

const client = new FrequenceSudClient('https://www.frequence-sud.fr/rss/ext_display/laplageinfo');
const villeRepository = new VilleRepository(client);
const meteoRepository = new MeteoRepository(client);
const plageRepository = new PlageRepository(client, villeRepository, meteoRepository);
let resolveSourceNodesCreation: (value: void) => void;

const sourceNodesCreation = new Promise<void>((resolve) => {
  resolveSourceNodesCreation = resolve;
});

export const sourceNodes: GatsbyNode['sourceNodes'] = async (sourceNodesArgs) => {
  // If a model property is a class it won't make it intact to the data layer.
  // The property will just disappear without warning and relationship won't work.
  const createNodeFromDataModel = createCreateNodeFromDataModel(sourceNodesArgs);

  const villes = await villeRepository.findAll();
  villes.forEach((ville: Ville) => createNodeFromDataModel(ville, 'Ville'));

  const plages = await plageRepository.findAll();
  plages.forEach((plage: Plage) => createNodeFromDataModel({ ...plage }, 'Plage'));

  resolveSourceNodesCreation();

  return;
};

const isFileSystemNode = (node: any): node is FileSystemNode => node?.internal.type === 'File';

export const onCreateNode: GatsbyNode['onCreateNode'] = async ({ node, getNode, actions, reporter }) => {
  const { createNodeField } = actions;
  if (node.internal.type === 'MarkdownRemark') {
    if (node.parent) {
      const parentNode = await getNode(node.parent);
      if (isFileSystemNode(parentNode)) {
        const pathParts = parentNode.relativeDirectory.split(path.sep);
        reporter.log(
          `MarkdownRemark parent node "${parentNode.name}" has the following path parts: ${pathParts.join(', ')}`
        );

        await sourceNodesCreation;
        reporter.log(`Resuming "${parentNode.name}" onCreateNode callback.`);

        if (pathParts.length === 2) {
          // Then it's a plage.
          const plageId = Plage.computeId(parentNode.name);
          const plageNode = await getNode(plageId);
          if (plageNode) {
            createNodeField({
              node,
              name: `plage___NODE`,
              value: Plage.computeId(parentNode.name),
            });
          } else {
            reporter.warn(`Plage with id "${plageId}" doesn't exist.`);
          }
          createNodeField({
            node,
            name: `plage___NODE`,
            value: Plage.computeId(parentNode.name),
          });
        } else if (pathParts.length === 1) {
          // Then it's a ville.
          const villeId = Ville.computeId({ departement: pathParts[0], nom: parentNode.name });
          const villeNode = await getNode(villeId);
          if (villeNode) {
            createNodeField({
              node,
              name: `ville___NODE`,
              value: villeId,
            });
          } else {
            reporter.warn(`Ville with id ${villeId} doesn't exist.`);
          }
        }
      }
    }
  }
};

export const createSchemaCustomization: GatsbyNode['createSchemaCustomization'] = ({ actions }) => {
  const { createTypes } = actions;
  createTypes([
    `type Ville implements Node {
      plages: [Plage] @link(by: "ville.nom", from: "nom"),
    }`,
    `type PlageMeteo {
      alertes: [String!]!
      typeDeDrapeau: Int
      merEtat: String
      eau: PlageMeteoEau
      commentaire: String
    }`,
    `type PlageMeteoEau {
      qualite: String
    }`,
  ]);
};
