# Drapeaux de baignades

- 0 > pas de drapeau / pas de donnée
- 1 > vert (ex: https://www.frequence-sud.fr/img/navigation/ux/meteo/flag2_1.png)
  - baignade surveillée sans danger apparent
  - niveau de risque: faible
- 2 > jaune
  - baignade surveillée avec danger limité ou marqué
  - niveau de risque: marqué ou limité
- 3 > rouge
  - baignade interdite
  - niveau de risque: fort
- 4 > violet / pollution
  - pollution ou présence d'espèces aquatiques dangereuses, zone marine et sous-marine protégées (faune aquatique, récifs, ...)
  - niveau de risque: ?
- ?
  - Zone de baignade surveillée pendant les horaires d’ouverture du poste de secours

> __Note :__ Historiquement les drapeaux étaient triangulaires, voir par exemple :
> https://www.frequence-sud.fr/img/navigation/ux/meteo/flag_1.png
> Le drapeau orange est également devenu jaune.

## Resources

- https://france3-regions.francetvinfo.fr/occitanie/herault/montpellier/nouveaux-drapeaux-sur-les-plages-la-signalisation-des-zones-et-conditions-de-baignade-change-sur-le-littoral-2536112.html
- https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000045100440#:~:text=%C2%AB%203%C2%B0%20Deux%20drapeaux%20identiques,longueur%20minimale%20de%20900%20mm.
- https://www.service-public.fr/particuliers/actualites/A15715
- https://www.sports.gouv.fr/IMG/pdf/fiche_signale_tique_des_baignadesversion_finale2_003_1_.pdf
