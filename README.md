# La Plage

Service d’information sur la qualité de l’eau de baignade, la météo, la couleur du drapeau, les services disponibles...

> https://laplage.info/

## Données météo

### Origine
Les données météo sont fournies par l'API __Fréquence-Sud__ pour le département du __Var__ et des __Bouches-du-Rhône__.

Les données fournies par __Fréquence-Sud__ sont agrégées depuis différentes sources :
- des __collectivités locales__ qui remplissent manuellement ces données depuis le *backoffice* de __Fréquence-Sud__ : https://www.frequence-sud.fr/pro/new_user_pro.php
- de __services tiers__ pour les données atmosphériques :
    - https://openweathermap.org/
    - https://stormglass.io/

### Données ouvertes

Les __données météos des plages de l'année 2022__ ont également été mises à disposition sur la plateforme open data de la __Région Sud Provence-Alpes-Côte d’Azur__, __DataSUD__ :
https://trouver.datasud.fr/dataset/meteo-des-plages-2022

> La structure de donnée est décrite ici :
> https://gitlab.com/ifestival/widget-meteo-des-plages#nomenclature-d%C3%A9tail-des-donn%C3%A9es

## *Backend*

Le service __LaPlage__ est actuellement dépendant des données fournies par l'API __Fréquence-sud__.

> __Note :__ une version simplifiée du backend de __Fréquence-sud__ est disponible ici :
> https://gitlab.com/ifestival/widget-meteo-des-plages

Pour être ré-utilisé et déployé vers un autre territoire (ou pour un passage au national) il sera nécessaire de créer un backend.
Ce backend devra pouvoir fournir les services suivants :
- un *backoffice*, permettant la __saisie__ :
    - des __relevés météo des plages__ et le __signalement d'incidents__ par les __agents communaux__ et/ou le __grand public__
    - (*optionnelle*) des __contenus texte__, __photos__, permettant d'__éditorialiser__ les pages relatives aux collectivités et aux plages associées.
- une __API__ (Rest ou autre) permettant d'__exposer ces données météo__ pour les afficher sur un *frontend* __LaPlage__

> L'outil [Directus](https://github.com/directus/directus) a été identifié comme pertinent pour collecter et rendre accessible les données des collectivités via une API.

## Gestion du contenu

### Plage

La **page** d'une **plage** spécifique peut être customisée via la création d'un fichier [markdown](https://fr.wikipedia.org/wiki/Markdown#Formatage) dans le dossier `src/markdown-pages/`.

Le modèle à suivre est `src/markdown-pages/{departement}/{slug-du-nom-de-la-ville}/{slug-du-nom-de-la-plage}.md`

> Un **slug** est la version d'une chaîne de caractère où sont remplacés :
>
> - les majuscules par des minuscules
> - les caractères accentués par leurs équivalents non accentués
> - les espaces et les caractères spéciaux par des tirets **-**
>
> *Pour l'implémentation exacte de l'algorithme de _slugification_ voir :*
> https://github.com/sindresorhus/slugify/blob/main/index.js#L23-L74

**Exemple :**

Pour customiser la page de la plage **Plage Prado Nord** qui se situe à **Marseille** dans le **83** il conviendra de créer le fichier dans le dossier `src/markdown-pages/83/marseille/plage-prado-nord.md` :

```markdown
---
title: La plage du Prado nord
description: Station balnéaire d'exception. Venez vous baignez à Six-Fours-les-Plages
---

# La plage du Prado (Nord)

La plage du Prado est découpée en plusieurs plages. On parlera plutôt _des_ plages du **Prado**.
```

> Voir https://fr.wikipedia.org/wiki/Markdown#Formatage

### Ville

La **page** d'une **ville** spécifique peut être customisée via la création d'un fichier [markdown](https://fr.wikipedia.org/wiki/Markdown#Formatage) dans le dossier `src/markdown-pages/`.

Le modèle à suivre est `src/markdown-pages/{departement}/{slug-du-nom-de-la-ville}.md`

**Exemple :**

Pour customiser la page de la ville de **Six-Fours-les-Plages** dans le **83** il conviendra de créer le fichier dans le dossier `src/markdown-pages/83/six-fours-les-plages.md` :

```markdown
---
title: Six-Fours-les-Plages | Station balnéaire
description: La plage du Prado est découpée en plusieurs plages. On parlera plutôt des plages du Prado.
---

# Station balnéaire Six-Fours-les-Plages

Six-Fours-les-Plages est une commune française qui fait partie de la métropole Toulon Provence Méditerranée, dans le département du Var, en région Provence-Alpes-Côte d’Azur. Son nom viendrait de “six forts” car l’imposante muraille qui cloisonne le village de départ et son fort (Fort de Six-fours) est constituée de plusieurs tours. Elle forme une péninsule avec La Seyne sur mer et Saint Mandrier protégeant ainsi la rade de Toulon.

## Localisation

Située au bord de la méditerranée, la ville compte 32 048 habitants en 2021 et est la septième plus peuplée du département. Sa population a augmenté fortement à partir des années 60 grâce au tourisme et à ses magnifiques plages. Cette augmentation est désormais freinée et les Six-Fournais connaissent aujourd’hui un vrai problème de vieillissement de leur population. Mais la commune au code postal 83129 reste tout de même une station balnéaire prisée par les touristes du monde entier.
```

> __Note sur le découpage du contenu :__
> Le contenu est disposé pour partie avant les données météo et le reste après les données météo.
> Tout le contenu présent avant le second titre (titre 2 ou `h2`) apparaîtra avant les données météo et le reste apparaîtra après.

## Statistiques

Des données sont collectées via **Matomo** et accessible depuis https://matomo.collectivite.org/

## Développement

```shell
npm install
npm run develop
```

> Your site is now running at http://localhost:8000!

## Déploiement

Le site est construit et déployé *via* un *pipeline* __GitLab__ (Voir [`.gitlab-ci.yml`](.gitlab-ci.yml).)

Le site ainsi construit est statique et peut être hébergé sur les [pages __Gitlab__](https://incubateur-territoires.gitlab.io/france-relance/meteo-des-plages/la-plage/).

> Il est __entièrement reconstruit toutes les 3h__ grâce à une [tâche planifiée](/incubateur-territoires/france-relance/meteo-des-plages/la-plage/-/pipeline_schedules).
> Le déploiement peut également être lancé manuellement depuis la [page listant les *pipelines*](/incubateur-territoires/france-relance/meteo-des-plages/la-plage/-/pipelines) grâce au bouton "*__Run pipeline__*".

## API __Fréquence-Sud__

### Modèle de donnée
```mermaid
erDiagram
    MeteoVille {
        float temperatureAir
        float temperatureAtmospherique
    }
    TemperatureEauVille {
        float temperatureEau
    }
    LieuPlage {
        string nomVille
        int departementVille
    }
    MeteoPlage {
        string infosMaritimes
    }
    MeteoVille |o--o| Ville : "alimente automatiquement la météo"
    TemperatureEauVille ||--|{ Ville : "alimente automatiquement la météo"
    Ville ||--|{ LieuPlage : "est composée de"
    LieuPlage }|..|{ MeteoPlage : "est alimenté manuellement (collectivité) par une"
    MeteoPlage }|..|{ Meduses : "peut avoir des signalements de"
```

> __Notes :__
> - Historiquement un lieu n'était pas necessairement une plage.
> - __Signalement de méduse :__ Une tâche planifiée (Cron) met à jour les signalements des dernières 48h. Ces signalements sont effectués soit par les communes, soit par des utilisateurs.
> - __Température de l'air :__ Générée à partir de la méteo aérienne liée à la commune.
> - __Infos maritimes :__ Notamment la température de l'eau.
    > En théorie, les mise à jour ont lieu entre 9h-12h.

### Backend
> https://www.frequence-sud.fr/rss/ext_display/sanary/plage_detail.php?id_plage=3815

```mermaid
flowchart TB
    Collecte-->Stockage
    Stockage-->PréparationDonnées
    PréparationDonnées-->AffichageDonnées
```

1. Collecte et stockage
2. Préparation de la donnée et affichage :
- récuperation données satellite "secours" température aériennes et de l'eau via un service externe
- affichage intelligent de la donnée

### Liste des points d'entrées

#### Données permanentes des plages

> id lieu, infos sur plage, visuel, coordonnées, adresse, ville

https://www.frequence-sud.fr/rss/ext_display/laplageinfo/liste_lieux.php

> __Exemple d'URLs possibles pour une API Rest :__
> `/lieux.php` -> `/lieux`
> `/lieux.php?id_lieu=5264` -> `/lieux/5264`

__Exemple de réponse :__
```json
[
 "data": {
    {
        "nom": "Plage de la Sylvabelle",
        "id_lieu": 2544,
        "latitude": 43.187912,
        "longitude": 6.574569,
        "ville": "La Croix-Valmer",
        "departement": 83,
        "telephone": null,
        "email": null,
        "tag_lieu_cat": ["nature", "plage"],
    },
  }
]
```

#### Données météos des plages

https://www.frequence-sud.fr/rss/ext_display/laplageinfo/flux.php
> id lieu, mode complet ou dégradé, temp eau + air, météo, drapeau, qualité eau...



Les température sont en __°celsius arrondi à l'entier__.
La __vitesse du vent en km/h__.


__Exemple de réponse :__
`flux.php`
```json
[
    "data": {
        "meteo_plage_id_lieu": 2393,
        "meteo_plage_maj": "2022-04-29 16:09:01",
        "meteo_plage_drapeau": 0,
        "meteo_plage_temperature": 14,
        "meteo_plage_etat_mer": "non disponible",
        "meteo_plage_alert": null,
        "meteo_plage_comment": "",
        "meteo_plage_qual": "Absence d'analyse"
    }
]
```

> Pour la description des drapeaux de baignades et de leurs codes voir : [Drapeaux de baignades](doc/DRAPEAUX_BAIGNADES.md)

#### Données des villes
https://www.frequence-sud.fr/rss/ext_display/laplageinfo/liste_villes.php
```json
[
    "data": {
        {
            "vlatitude": 43.766667,
            "longitude": 6.25,
            "nom": "Aiguines",
            "departement": 83,
            "codepostal": 83630
        },
    }
]
```

## Ressources

### Accompagnemnt France-Relance

D'autres livrables issus de l'accompagnement __France-Relance__ devraient également être mis à disposition.

### APIs Météos

Différentes APIs ont historiquement été utilisées par __Fréquence-Sud__ pour récuperer des données et pictogrammes de la météo aérienne.

#### StormGlass

> https://docs.stormglass.io/#/weather

__Avantages :__
- Température de l'eau
- Hauteur des vagues (:warning: attention à la fiabilité)

__Inconvenients :__
- Pas d'icônes

#### OpenWeatherMap

> https://openweathermap.org/weather-conditions

__Avantages :__
- Super pictos :+1:
- Recherche par nom de ville

__Inconvenients :__
- Besoin de stocker l'identfiant `openmet` pour appels ultérieurs

> *Exemple d'icones utilisées par __Fréquence-Sud__ :*
> - "clear sky" ![clear sky](https://www.frequence-sud.fr/img/navigation/ux/meteo/icon2/d800.png)
> - "Clouds	few clouds: 11-25%" ![Clouds few clouds: 11-25%](https://www.frequence-sud.fr/img/navigation/ux/meteo/open/802.png)

### Gatsby

Ce site est basé sur le *framework* __GatsbyJs__ et est développé en __Typescript__.

- [Documentation](https://www.gatsbyjs.com/docs/)
- [__i18n__ (ou internationalisation)](https://www.gatsbyjs.com/docs/how-to/adding-common-features/localization-i18n/)
- [Tutoriaux](https://www.gatsbyjs.com/tutorial/)

## Licence

[AGP v3](./LICENSE)